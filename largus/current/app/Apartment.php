<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Apartment extends Model
{
    const STATUS_FREE   = 'free';
    const STATUS_BOOKED = 'booked';
    const STATUS_SOLD   = 'sold';
    const STATUS_HOLD   = 'hold';

    public static function findBy($params) {
        $range_available = ['price', 'total_price', 'area', 'floor',];
        $equal_available = ['type', 'housing', 'porch', 'count_room', 'room_number', 'view', 'status', 'room_number_in_floor'];

        $rv = self::where([]);
        if(isset($params['room_number'])) {
            return $rv->where('room_number', $params['room_number']);
        }

        foreach($range_available as $field) {
            if(isset($params[$field . '_from']) && !isset($params[$field . '_to'])) {
                $rv = $rv->where($field, '>=', $params[$field . '_from']);
            }

            if(!isset($params[$field . '_from']) && isset($params[$field . '_to'])) {
                $rv = $rv->where($field, '<=', $params[$field . '_to']);
            }

            if(isset($params[$field . '_from']) && isset($params[$field . '_to'])) {
                $rv = $rv->where($field, '>=', $params[$field . '_from'])->where($field, '<=', $params[$field . '_to']);
            }
        }

        foreach($equal_available as $field) {
            if(isset($params[$field])) {
                if($field == 'view') {
                    $params[$field]--;
                }
                $rv = $rv->where($field, $params[$field]);
            }
        }

        return $rv;
    }

    public static function byRoomNumber($roomNumber)
    {
        return self::where('room_number', $roomNumber)->first();
    }

    public static function fromExcel($fields)
    {

    }

    public static function getBookedByLead($lead)
    {
        $result = [];
        $data = Apartment::whereIn('status', array(Apartment::STATUS_BOOKED, Apartment::STATUS_SOLD))->where('lead', $lead)->get();
        foreach($data as $item) {
            if($item->booked_for) {
                $item->booked_for = date('d.m.Y', strtotime($item->booked_for));
            }
            $result[] = $item;
        }

        return $result;
    }


}
