<?php
/**
 * Created by PhpStorm.
 * User: shaelf
 * Date: 15.03.16
 * Time: 6:27
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Apartment;
use App\Api;
use Symfony\Component\HttpFoundation\Response;
use Log;


class HookController extends Controller
{
    public function status(Request $request) {
        $params = $request->get('leads');
        $status = $params['status'][0]['status_id'];
        $lead = $params['status'][0]['id'];
        if($status == 143) {
            $reserved = Apartment::where('lead', $lead)->where('status', Apartment::STATUS_BOOKED)->get();
            if($reserved) {
                foreach($reserved as $apartment) {
                    $apartment->status = Apartment::STATUS_FREE;
                    $apartment->save();
                }
            }
        }

        return \Response::json([]);
    }
}