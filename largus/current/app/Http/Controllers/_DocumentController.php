<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Apartment;
use App\Document;
use App\Api;
use Symfony\Component\HttpFoundation\Response;
use Log;
use PhpOffice\PhpWord\TemplateProcessor;

class DocumentController extends Controller
{

    public function generate(Request $request)
    {
        $data = $request->all();
        $doc = new Document();

        $subDomain = 'laruscapital';
        $login = 'kalinina@laruscapital.com';
        $key = '0a711d08494279c3e3c773e4fdf17b40';

        $numberLead = $data['lead'];
        $contact = $data['contact'];

        $amo = new \AmoRestApi($subDomain, $login, $key);

        $contact = array_shift($amo->getContactsList(null, null, $contact, null,null,'all')['contacts']);
        $leadAmo = $amo->getLeadsList(null, null, $numberLead);
        $leadSearch = array_shift( $amo->getLeadsList(null, null, $numberLead)['leads']);
        $account = $amo->getAccountInfo();

        $day = date('d', $leadAmo['leads'][0]['date_create']);
        $year = date('Y', $leadAmo['leads'][0]['date_create']);

        switch (date('m', $leadAmo['leads'][0]['date_create'])){
            case 1: $moth='января'; break;
            case 2: $moth='февраля'; break;
            case 3: $moth='марта'; break;
            case 4: $moth='апреля'; break;
            case 5: $moth='мая'; break;
            case 6: $moth='июня'; break;
            case 7: $moth='июля'; break;
            case 8: $moth='августа'; break;
            case 9: $moth='сентября'; break;
            case 10: $moth='октября'; break;
            case 11: $moth='ноября'; break;
            case 12: $moth='декабря'; break;
        }

        $lead = Apartment::byLeadNumber($numberLead);
        $templateProcessor = new TemplateProcessor('Contract.docx');

        $name = $contact['name'];
        $arr = explode(" ", $name);
        $shortName = $arr[0].' '.mb_substr($arr[1],0,1,'UTF-8').'. '.mb_substr($arr[2],0,1,'UTF-8').'.';

        $builderCosts = money_format('%n', $lead->total_price*0.98);
        $builderCostsRub = explode(".", $builderCosts);
        $priceService = money_format('%n', $lead->total_price - $builderCosts);
        $priceService = explode(".", $priceService);

        $dateOfBirth = explode("-", $doc->searchField($contact, 'Дата рождения'));
        
        $payTimeAmo = $doc->searchField($leadSearch, 'Срок рассрочки (мес.)');
        if (($payTimeAmo == "") or ($payTimeAmo == 0)) {
        	$payTime = "5 (Пяти) рабочих дней"; 
        } else {
        	$monthForm = " месяцев ";
        	if ((($payTimeAmo % 10 == 1) and ($payTimeAmo != 11)) or ($payTimeAmo == 1)) {
        		$monthForm = " месяца ";
        	}
        	$payTime = $payTimeAmo.$monthForm;
        }
        
        $templateProcessor->setValue('agreement', $numberLead);
        $templateProcessor->setValue('sex', $doc->searchField($contact, 'Пол'));
        $templateProcessor->setValue('dateOfBirth',$dateOfBirth[2][0].$dateOfBirth[2][1]. '.'.$dateOfBirth[1].'.'.$dateOfBirth[0]);
        $templateProcessor->setValue('birthplace', $doc->searchField($contact, 'Место рождения'));
        $templateProcessor->setValue('passportID', $doc->searchField($contact, 'Номер паспорта'));
        $templateProcessor->setValue('issued', $doc->searchField($contact, 'Выдан'));
        $templateProcessor->setValue('code', $doc->searchField($contact, 'Код подразделения'));
        $templateProcessor->setValue('register', $doc->searchField($contact, 'Регистрация'));
        $templateProcessor->setValue('snils', $doc->searchField($contact, 'Снилс'));
        $templateProcessor->setValue('paytime', $payTime);
        $templateProcessor->setValue('p-h', $lead->porch);
        $templateProcessor->setValue('f-r', $lead->floor);
        $templateProcessor->setValue('r-n', $lead->room_number);
        $templateProcessor->setValue('c-r', $lead->count_room);
        $templateProcessor->setValue('o-s', $lead->total_area);
        $templateProcessor->setValue('l-s', $lead->living_area);
        $templateProcessor->setValue('type', $lead->type);
        $templateProcessor->setValue('totalPrice', $lead->total_price);
        $templateProcessor->setValue('totalPriceInString', $doc->num2str($lead->total_price)['sum']);
        $templateProcessor->setValue('currency', $doc->num2str($lead->total_price)['currency']);
        $templateProcessor->setValue('price', $lead->price);
        $templateProcessor->setValue('priceInString', $doc->num2str($lead->price)['sum']);
        $templateProcessor->setValue('price', $lead->price);
        $templateProcessor->setValue('priceServiceRubles', $priceService[0]);
        $templateProcessor->setValue('priceServiceKopeck', $priceService[1]);
        $templateProcessor->setValue('builderCostsRubles', $builderCostsRub[0]);
        $templateProcessor->setValue('builderCostsKopeck', $builderCostsRub[1]);
        $templateProcessor->setValue('name', $name);
        $templateProcessor->setValue('phone', $doc->searchField($contact, 'Телефон'));
        $templateProcessor->setValue('email', $doc->searchField($contact, 'Email'));
        $templateProcessor->setValue('a-a', $lead->area);
        $templateProcessor->setValue('shortName', $shortName);
        $templateProcessor->setValue('codeOfSubdivision', $doc->searchField($contact, 'Код подразделения'));
        $templateProcessor->setValue('mailAddress', $doc->searchField($contact, 'Почтовый адрес'));
        $templateProcessor->setValue('day', $day);
        $templateProcessor->setValue('moth', $moth);
        $templateProcessor->setValue('year', $year);


        $picture = file_get_contents('http://timpark.ru/assets/images/apts/'.$lead->housing.'-'.$lead->room_number.'.png');
        $image = tempnam(sys_get_temp_dir(), '').'.png';
        file_put_contents($image, $picture);

        $templateProcessor->replaceStrToImg('image', [$image], ['width' => 580, 'height' => '360']);
        
        $floor = '';
        switch ($lead->floor){
        	case 1: 
        		$floor='1floor'; 
        		break;
        	case 2: 
        	case 3:
        	case 4:
        	case 5:
        	case 6:
        	case 7:
        		$floor='2-7floor'; 
        		break;
        	case 8: 
        	case 9:
        	case 10:
        	case 11:
        	case 12:
        	case 13: 
        	case 14:
        		$floor='8-14floor'; 
        		break;
        	case 15: 
        	case 16:
        	case 17:
        	case 18:
        	case 19:
        	case 20:
        		$floor='15-20floor'; 
        		break;
        	case 21: 
        		$floor='21floor';  
        		break;
        	case 22: 
        		$floor='22floor'; 
        		break;
        }
        
        $picturePlan = file_get_contents('plans/'.$floor.'_'.$lead->porch.'_'.$lead->room_number_in_floor.'.png');
        $imagePlan = tempnam(sys_get_temp_dir(), '').'.png';
        file_put_contents($imagePlan, $picturePlan);
        
        $templateProcessor->replaceStrToImg('planImage', [$imagePlan], ['width' => 580, 'height' => '360']);

        $prefix = 'Contr_';
        $temp_file = tempnam(sys_get_temp_dir(), $prefix).'.docx';

        $templateProcessor->save($temp_file);
       
        return response()->download($temp_file);
        
    }
}