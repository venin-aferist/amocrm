<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

abstract class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * @var \AmoRestApi
     */
    protected $amo = null;

    /**
     * @return \AmoRestApi
     */
    protected function getAmo() {
        if($this->amo === null) {
            $subDomain = 'laruscapital';
            $login = 'kalinina@laruscapital.com';
            $key = '0a711d08494279c3e3c773e4fdf17b40';

            $this->amo = new \AmoRestApi($subDomain, $login, $key);
        }

        return $this->amo;
    }
}
