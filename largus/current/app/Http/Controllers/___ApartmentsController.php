<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Apartment;
use App\Api;
use Illuminate\Support\Facades\Log;
use Symfony\Component\HttpFoundation\Response;

class ApartmentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $result = [];
        $apartments = Apartment::orderBy('floor', 'DESC')->get();
        foreach($apartments as $item) {
            if($item->booked_for) {
                $item->booked_for = date('d.m.Y', strtotime($item->booked_for));
            }
            $result[] = $item;
        }
        return \Response::json($result);
    }

    public function search(Request $request)
    {
        $result = [];
        if($request->get('result_count')) {
            $result = Apartment::findBy($request->all())->orderBy('floor', 'DESC')->count();
        } else {
            $data = Apartment::findBy($request->all())->orderBy('floor', 'DESC')->get();
            foreach($data as $item) {
                if($item->booked_for) {
                    $item->booked_for = date('d.m.Y', strtotime($item->booked_for));
                }
                $result[] = $item;
            }
        }
//        $result = Apartment::findBy($request->all())->getSql();

        return \Response::json($result);
    }

    public function booked(Request $request, $lead)
    {
        return \Response::json(Apartment::getBookedByLead($lead));
    }

    public function import(Request $request, Response $response)
    {
        $file = $request->file('import');
        $data = file_get_contents($file->getPath() . '/' . $file->getFilename());
        foreach(explode("\n", $data) as $string) {
            $fields = explode(';', $string);
            if(((int) $fields[3]) > 0) {
                $exist = Apartment::byRoomNumber($fields[3]);

                $price      = (int) str_replace(['₽', "В ", ' ', " "], '', $fields[9]);
                $totalPrice = (int) str_replace(['₽', "В ", " ", ' '], '', $fields[8]);

                if($exist) {
                    if($exist->status == Apartment::STATUS_FREE || $exist->status == Apartment::STATUS_HOLD) {
                        $exist->price = $price;
                        $exist->total_price = $totalPrice;
                    }
                    $exist->new_total_price = $totalPrice;
                    $exist->new_price = $price;


                    $exist->housing = $fields[0];
                    $exist->porch = $fields[1];
                    $exist->floor = $fields[2];
                    $exist->room_number_in_floor = $fields[4];
                    $exist->count_room = $fields[5];
                    $exist->area = str_replace(',', '.', $fields[6]);
                    $exist->view = $fields[11];
                    $exist->type = $fields[10];
                    $exist->save();
                } else {
                    //вид последний
                    $apartment = new Apartment();
                    $apartment->housing = $fields[0];
                    $apartment->porch = $fields[1];
                    $apartment->floor = $fields[2];
                    $apartment->room_number = $fields[3];
                    $apartment->room_number_in_floor = $fields[4];
                    $apartment->count_room = $fields[5];
                    $apartment->view = $fields[11];
                    $apartment->area = $fields[6];
                    $apartment->status = $this->getStatus($fields[7]);
                    $apartment->total_price = $totalPrice;
                    $apartment->price = $price;
                    $apartment->type = $fields[10];
                    $apartment->new_total_price = $totalPrice;
                    $apartment->new_price = $price;
                    $apartment->save();
                }
            }
        }

        return redirect()->back();
    }

    public function exports(Request $request)
    {
        $data = Apartment::all();
        $result = array();
        foreach($data as $item) {
            $result[] = array(
                'building_number'  => $item->housing,
                'section_number'   => $item->porch,
                'floor_number'     => $item->floor,
                'number'           => $item->room_number,
                'number_on_floor'  => $item->room_number_in_floor,
                'room_count'       => $item->count_room,
                'square'           => $item->area,
                'status'           => $this->statusAsNumber($item->status),
                'total_cost'       => $item->total_price,
                'cost_per_meter'   => $item->price,
                'type'             => $item->type,
                'note'             => ''
            );
        }

        return \Response::json($result);
    }

    public function form(Request $request)
    {
        return view('apartments#form');
    }

    public function status(Request $request)
    {
        $apartment = Apartment::find($request->get('id'));

        $leadId = ($apartment->lead > 0) ? $apartment->lead : $request->get('lead');

        if($request->get('status') == Apartment::STATUS_BOOKED) {
            if (Apartment::where('lead', $request->get('lead'))->count() >= 3) {
                return \Response::json(array('error' => 'Забронированно максимальное количество'));
            }

            $timestamp = strtotime($request->get('date'));
            $date = date('Y-m-d', $timestamp);
            $apartment->booked_for = $date;
            $apartment->lead = $request->get('lead');
            $apartment->status = $request->get('status');
            $apartment->deal_name = $request->get('deal_name');
            $apartment->save();
        } elseif($request->get('status') == Apartment::STATUS_SOLD) {
            $apartment->deal_name = '';
            $apartment->status = Apartment::STATUS_SOLD;
            $apartment->save();
        } else {
            $apartment->status = $request->get('status');
            $apartment->price = $apartment->new_price;
            $apartment->total_price = $apartment->new_total_price;
            $apartment->lead = '';
            $apartment->deal_name = '';
            $apartment->save();
        }

        $price = 0;
        foreach(Apartment::getBookedByLead($leadId) as $a) {
            $price += $a->total_price;
        }

        Log::info($leadId);
        $updatePrice['update'] = [[
            'price' => $price,
            'id'    => $leadId,
            'last_modified' => time() + 10
        ]];

        Log::info($updatePrice);
        $this->getAmo()->setLeads($updatePrice);

        return \Response::json($apartment);
    }

    private function statusAsNumber($status)
    {
        switch($status) {
            case Apartment::STATUS_BOOKED:
                return 2;
                break;
            case Apartment::STATUS_FREE:
                return 1;
                break;
            case Apartment::STATUS_SOLD:
                return 0;
                break;
            case Apartment::STATUS_HOLD:
                return 0;
                break;
        }
    }

    public function change(Request $request)
    {
        foreach(Apartment::all() as $apartment) {
            if($apartment->room_number > 80) {
                $apartment->room_number = $apartment->room_number -2;
                $apartment->save();
            }
        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    private function getStatus($id)
    {
        return ($id == '1') ? Apartment::STATUS_FREE : Apartment::STATUS_BOOKED;
    }
}
