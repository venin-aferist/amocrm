<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\CarPlace;
use App\Api;
use Illuminate\Support\Facades\Log;
use Symfony\Component\HttpFoundation\Response;

class CarPlaceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $result = [];
        $CarPlaces = CarPlace::orderBy('id')->get();
        foreach($CarPlaces as $item) {
            $result[] = $item;
        }
        return \Response::json($result);
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function search(Request $request)
    {
        $result = [];
        if($request->get('result_count')) {
            $result = CarPlace::findBy($request->all())->orderBy('id')->count();
        } else {
            $data = CarPlace::findBy($request->all())->orderBy('id')->get();
            foreach($data as $item) {
                $result[] = $item;
            }
        }

        return \Response::json($result);
    }

    /**
     * @param Request $request
     * @param Response $response
     */
    public function import(Request $request, Response $response)
    {
        $file = $request->file('import');
        if (($handle = fopen($file->getPath() . '/' . $file->getFilename(), "r")) !== FALSE) {

            $flatsArray = explode("\n", file_get_contents($file));
            foreach ($flatsArray as $flat) {
                $fields = explode(";", $flat);
                if (count($fields) > 4) {
                    if(is_numeric($fields[0])) {
                        $exist = CarPlace::where('unique_num', $fields[1])->first();
                        if($exist) {
                            $exist->floor = $fields[0];
                            $exist->unique_num = $fields[1];
                            $exist->type = $fields[2];
                            $exist->area = str_replace(',', '.', $fields[3]);
                            $exist->price = (int) str_replace(['₽', "В ", ' ', " "], '', $fields[4]);
                            $exist->status = $fields[5];
                            $exist->save();
                        } else {
                            $CarPlace = new CarPlace();
                            $CarPlace->floor = $fields[0];
                            $CarPlace->unique_num = $fields[1];
                            $CarPlace->type = $fields[2];
                            $CarPlace->area = str_replace(',', '.', $fields[3]);
                            $CarPlace->price = (int) str_replace(['₽', "В ", ' ', " "], '', $fields[4]);
                            $CarPlace->status = $fields[5];
                            $CarPlace->save();
                        }

                    }
                }
            }
            fclose($handle);
        }

//        return redirect()->back();
    }


    /**
     * @param Request $request
     * @param $lead
     * @return mixed
     */
    public function booked(Request $request, $lead)
    {
        return \Response::json(CarPlace::getBookedByLead($lead));
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function status(Request $request)
    {
        $carplace = CarPlace::find($request->get('id'));
        $carplace->status = $request->get('status');
        if (!empty($request->get('lead')) && $request->get('status') == 0) {
            $carplace->lead = $request->get('lead');
        }
        $carplace->save();
        return \Response::json($carplace);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function form(Request $request)
    {
        return view('car_place.form');
    }


}
