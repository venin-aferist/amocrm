<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Boxroom;
use App\Api;
use Illuminate\Support\Facades\Log;
use Symfony\Component\HttpFoundation\Response;

class BoxroomController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $result = [];
        $Boxrooms = Boxroom::orderBy('id')->get();
        foreach($Boxrooms as $item) {
            $result[] = $item;
        }
        return \Response::json($result);
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function search(Request $request)
    {
        $result = [];
        if($request->get('result_count')) {
            $result = Boxroom::findBy($request->all())->orderBy('id')->count();
        } else {
            $data = Boxroom::findBy($request->all())->orderBy('id')->get();
            foreach($data as $item) {
                $result[] = $item;
            }
        }
//        $result = Boxroom::findBy($request->all())->getSql();

        return \Response::json($result);
    }

    /**
     * @param Request $request
     * @param Response $response
     */
    public function import(Request $request, Response $response)
    {
        $file = $request->file('import');
        if (($handle = fopen($file->getPath() . '/' . $file->getFilename(), "r")) !== FALSE) {

            //while (($fields = fgetcsv($handle, 1000, ";")) !== FALSE) {
            $flatsArray = explode("\n", file_get_contents($file));
            foreach ($flatsArray as $flat) {
                $fields = explode(";", $flat);
                if (count($fields) > 4) {
                    if(is_numeric($fields[0])) {
                        $exist = Boxroom::where('unique_num', $fields[1])->first();
                        if($exist) {
                            $exist->porch = $fields[0];
                            $exist->unique_num = $fields[1];
                            $exist->area = str_replace(',', '.', $fields[2]);
                            $exist->price = (int) str_replace(['₽', "В ", ' ', " "], '', $fields[3]);
                            $exist->total_price = (int) str_replace(['₽', "В ", ' ', " "], '', $fields[4]);
                            $exist->status = $fields[5];

                            $exist->save();
                        } else {
                            $Boxroom = new Boxroom();
                            $Boxroom->porch = $fields[0];
                            $Boxroom->unique_num = $fields[1];
                            $Boxroom->area = str_replace(',', '.', $fields[2]);
                            $Boxroom->price = (int) str_replace(['₽', "В ", ' ', " "], '', $fields[3]);
                            $Boxroom->total_price = (int) str_replace(['₽', "В ", ' ', " "], '', $fields[4]);
                            $Boxroom->status = $fields[5];
                            $Boxroom->save();
                        }

                    }
                }
            }
            fclose($handle);
        }

//        return redirect()->back();
    }

    /**
     * @param Request $request
     * @param $lead
     * @return mixed
     */
    public function booked(Request $request, $lead)
    {
        return \Response::json(Boxroom::getBookedByLead($lead));
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function status(Request $request)
    {
        $boxroom = Boxroom::find($request->get('id'));
        $boxroom->status = $request->get('status');
        if (!empty($request->get('lead')) && $request->get('status') == 0) {
            $boxroom->lead = $request->get('lead');
        }
        $boxroom->save();
        return \Response::json($boxroom);
    }


    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function form(Request $request)
    {
        return view('boxroom.form');
    }


}
