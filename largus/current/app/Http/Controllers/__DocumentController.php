<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Apartment;
use App\Document;
use App\Api;
use Symfony\Component\HttpFoundation\Response;
use Log;
use PhpOffice\PhpWord\TemplateProcessor;

class DocumentController extends Controller
{

    public function generate(Request $request)
    {
        $data = $request->all();
        $doc = new Document();

        $subDomain = 'laruscapital';
        $login = 'kalinina@laruscapital.com';
        $key = '0a711d08494279c3e3c773e4fdf17b40';

        $numberLead = $data['lead'];
        $contact = $data['contact'];
        $apartmentId = $data['apartment'];

        $amo = new \AmoRestApi($subDomain, $login, $key);

        $contact = array_shift($amo->getContactsList(null, null, $contact, null,null,'all')['contacts']);
        $lead = $amo->getLeadsList(null, null, $numberLead);
        $account = $amo->getAccountInfo();

        $day = date('d', $lead['leads'][0]['date_create']);
        $year = date('Y', $lead['leads'][0]['date_create']);

        switch (date('m', $lead['leads'][0]['date_create'])){
            case 1: $moth='января'; break;
            case 2: $moth='февраля'; break;
            case 3: $moth='марта'; break;
            case 4: $moth='апреля'; break;
            case 5: $moth='мая'; break;
            case 6: $moth='июня'; break;
            case 7: $moth='июля'; break;
            case 8: $moth='августа'; break;
            case 9: $moth='сентября'; break;
            case 10: $moth='октября'; break;
            case 11: $moth='ноября'; break;
            case 12: $moth='декабря'; break;
        }

//        $lead = Apartment::byLeadNumber($numberLead);
        $lead = Apartment::find($apartmentId);


        $templateProcessor = new TemplateProcessor('Contract.docx');

        $name = $contact['name'];
        $arr = explode(" ", $name);
        $shortName = $arr[0].' '.mb_substr($arr[1],0,1,'UTF-8').'. '.mb_substr($arr[2],0,1,'UTF-8').'.';

        $builderCosts = money_format('%n', $lead->total_price*0.98);
        $builderCostsRub = explode(".", $builderCosts);
        $priceService = money_format('%n', $lead->total_price - $builderCosts);
        $priceService = explode(".", $priceService);

        $dateOfBirth = explode("-", $doc->searchField($contact, 'Дата рождения'));
        $templateProcessor->setValue('agreement', $numberLead);
        $templateProcessor->setValue('sex', $doc->searchField($contact, 'Пол'));
        if(isset($dateOfBirth[1])) {
            $templateProcessor->setValue('dateOfBirth',$dateOfBirth[2][0].$dateOfBirth[2][1]. '.'.$dateOfBirth[1].'.'.$dateOfBirth[0]);
        }
        $templateProcessor->setValue('birthplace', $doc->searchField($contact, 'Место рождения'));
        $templateProcessor->setValue('passportID', $doc->searchField($contact, 'Номер паспорта'));
        $templateProcessor->setValue('issued', $doc->searchField($contact, 'Выдан'));
        $templateProcessor->setValue('code', $doc->searchField($contact, 'Код подразделения'));
        $templateProcessor->setValue('register', $doc->searchField($contact, 'Регистрация'));
        $templateProcessor->setValue('snils', $doc->searchField($contact, 'Снилс'));
        $templateProcessor->setValue('p-h', $lead->porch);
        $templateProcessor->setValue('f-r', $lead->floor);
        $templateProcessor->setValue('r-n', $lead->room_number);
        $templateProcessor->setValue('c-r', $lead->count_room);
        $templateProcessor->setValue('totalPrice', $lead->total_price);
        $templateProcessor->setValue('totalPriceInString', $doc->num2str($lead->total_price)['sum']);
        $templateProcessor->setValue('currency', $doc->num2str($lead->total_price)['currency']);
        $templateProcessor->setValue('price', $lead->price);
        $templateProcessor->setValue('priceInString', $doc->num2str($lead->price)['sum']);
        $templateProcessor->setValue('price', $lead->price);
        $templateProcessor->setValue('priceServiceRubles', $priceService[0]);
        $templateProcessor->setValue('priceServiceKopeck', $priceService[1]);
        $templateProcessor->setValue('builderCostsRubles', $builderCostsRub[0]);
        $templateProcessor->setValue('builderCostsKopeck', $builderCostsRub[1]);
        $templateProcessor->setValue('name', $name);
        $templateProcessor->setValue('phone', $doc->searchField($contact, 'Телефон'));
        $templateProcessor->setValue('email', $doc->searchField($contact, 'Email'));
        $templateProcessor->setValue('a-a', $lead->area);
        $templateProcessor->setValue('shortName', $shortName);
        $templateProcessor->setValue('codeOfSubdivision', $doc->searchField($contact, 'Код подразделения'));
        $templateProcessor->setValue('mailAddress', $doc->searchField($contact, 'Почтовый адрес'));
        $templateProcessor->setValue('day', $day);
        $templateProcessor->setValue('moth', $moth);
        $templateProcessor->setValue('year', $year);

        $picture = file_get_contents('http://timpark.ru/assets/images/apts/'.$lead->housing.'-'.$lead->room_number.'.png');
        $image = tempnam(sys_get_temp_dir(), '').'.png';
        file_put_contents($image, $picture);

        $templateProcessor->replaceStrToImg('image', [$image], ['width' => 580, 'height' => '360']);

        $prefix = 'Contr_';
        $temp_file = tempnam(sys_get_temp_dir(), $prefix).'.docx';

        $templateProcessor->save($temp_file);

        return response()->download($temp_file);
    }
}