<?php

Route::get('/', function () {
//    return view('welcome');
    redirect('http://timpark.ru');
});

Route::get('/apartment/search', 'ApartmentsController@search');
Route::post('/apartment/import', 'ApartmentsController@import');
Route::post('/apartment/addpay', 'ApartmentsController@addpay');
//Route::get('/apartment/change', 'ApartmentsController@change');
Route::get('/apartment/status', 'ApartmentsController@status');
Route::get('/apartment/booked/{lead}', 'ApartmentsController@booked');
Route::get('/apartment/export', 'ApartmentsController@exports');

// кладовые
Route::get('/boxroom/booked/{lead}', 'BoxroomController@booked');
Route::get('/boxroom', 'BoxroomController@index');
Route::get('/boxroom/search', 'BoxroomController@search');
Route::post('/boxroom/import', 'BoxroomController@import');
Route::get('/boxroom/form', 'BoxroomController@form');
Route::get('/boxroom/status', 'BoxroomController@status');


// машиноместа
Route::get('/car_place/booked/{lead}', 'CarPlaceController@booked');
Route::get('/car_place', 'CarPlaceController@index');
Route::get('/car_place/search', 'CarPlaceController@search');
Route::post('/car_place/import', 'CarPlaceController@import');
Route::get('/car_place/form', 'CarPlaceController@form');
Route::get('/car_place/status', 'CarPlaceController@status');


Route::resource('apartment', 'ApartmentsController');


Route::post('/hook/status', 'HookController@status');

Route::get('document', 'DocumentController@generate');

