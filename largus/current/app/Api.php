<?php
/**
 * Created by PhpStorm.
 * User: shaelf
 * Date: 09.03.16
 * Time: 13:04
 */

namespace App;

class Api {

    protected $login = 'kalinina@laruscapital.com';
    protected $hash  = '0a711d08494279c3e3c773e4fdf17b40';
    protected $domain = 'laruscapital';

    public static function connect() {
        return new Api();
    }

    public function addComment($leadId, $comment) {
        $request = [
            'request' => [
                'notes' => [
                    'add' => [
                        'element_type' => 1,
                        'element_id' => $leadId,
                        'note_type' => 4,
                        'text' => $comment
                    ]
                ]
            ]
        ];
        $this->response('notes/set', $request);
    }

    private function response($link, $data, $method = 'POST') {
        $link = 'https://' . $this->domain . '.amocrm.ru/private/api/v2/json/'. $link . '.json'.
            $curl=curl_init();
        curl_setopt($curl,CURLOPT_RETURNTRANSFER,true);
        curl_setopt($curl,CURLOPT_USERAGENT,'amoCRM-API-client/1.0');
        curl_setopt($curl,CURLOPT_URL, $link.
            '?USER_LOGIN=' . $this->login . '&USER_HASH=' . $this->hash);
        if (!empty($method))
        {
            curl_setopt($curl,CURLOPT_CUSTOMREQUEST,$method);
        }
        if (!empty($data))
        {
            curl_setopt($curl,CURLOPT_POSTFIELDS,json_encode($data));
        }
        curl_setopt($curl,CURLOPT_HTTPHEADER,array('Content-Type: application/json'));
        curl_setopt($curl,CURLOPT_HEADER,false);
        curl_setopt($curl,CURLOPT_COOKIEFILE,dirname(__FILE__).'/cookie.txt'); #PHP>5.3.6 dirname(__FILE__) -> __DIR__
        curl_setopt($curl,CURLOPT_COOKIEJAR,dirname(__FILE__).'/cookie.txt'); #PHP>5.3.6 dirname(__FILE__) -> __DIR__
        curl_setopt($curl,CURLOPT_SSL_VERIFYPEER,0);
        curl_setopt($curl,CURLOPT_SSL_VERIFYHOST,0);

        $out=curl_exec($curl); #Инициируем запрос к API и сохраняем ответ в переменную
        $code=curl_getinfo($curl,CURLINFO_HTTP_CODE); #Получим HTTP-код ответа сервера
        curl_close($curl); #Завершаем сеанс cURL
        return $out;
    }


}