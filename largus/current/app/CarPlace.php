<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CarPlace extends Model
{

    protected $table = 'car_place';
    public static function findBy($params) {
        $range_available = ['price', 'area'];
        $equal_available = ['type', 'status'];

        $rv = self::where([]);

        foreach($range_available as $field) {
            if(isset($params[$field . '_from']) && !isset($params[$field . '_to'])) {
                $rv = $rv->where($field, '>=', $params[$field . '_from']);
            }

            if(!isset($params[$field . '_from']) && isset($params[$field . '_to'])) {
                $rv = $rv->where($field, '<=', $params[$field . '_to']);
            }

            if(isset($params[$field . '_from']) && isset($params[$field . '_to'])) {
                $rv = $rv->where($field, '>=', $params[$field . '_from'])->where($field, '<=', $params[$field . '_to']);
            }
        }

        foreach($equal_available as $field) {
            if(isset($params[$field])) {
                if($field == 'view') {
                    $params[$field]--;
                }
                $rv = $rv->where($field, $params[$field]);
            }
        }

        return $rv;
    }

    /**
     * @param $lead
     * @return array
     */
    public static function getBookedByLead($lead)
    {
        $result = [];
        $data = CarPlace::where('status', 0)->where('lead', $lead)->get();
        foreach($data as $item) {
            if($item->booked_for) {
                $item->booked_for = date('d.m.Y', strtotime($item->booked_for));
            }
            $result[] = $item;
        }

        return $result;
    }

    public static function fromExcel($fields)
    {

    }

}
