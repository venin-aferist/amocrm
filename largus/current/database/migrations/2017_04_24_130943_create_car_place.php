<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCarPlace extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('car_place', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('floor'); // этаж
            $table->integer('unique_num'); //
            $table->string('type');
            $table->float('area');
            $table->integer('price');
            $table->integer('status');
            $table->integer('lead');

            $table->timestamps();

            $table->index('floor');
            $table->index('price');
            $table->index('area');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('car_place');
    }
}
