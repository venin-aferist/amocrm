<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApartmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('apartments', function (Blueprint $table) {
            $table->increments('id');
            $table->enum('status', ['free', 'booked', 'sold']);
            $table->integer('total_price');
            $table->integer('price');
            $table->integer('housing');
            $table->integer('porch');
            $table->integer('floor');
            $table->string('type');
            $table->integer('room_number');
            $table->integer('room_number_in_floor');
            $table->integer('count_room');
            $table->integer('view');
            $table->float('area');
            $table->date('booked_for');
            $table->integer('lead');

            $table->timestamps();

            $table->index('status');
            $table->index('total_price');
            $table->index('price');
            $table->index('housing');
            $table->index('porch');
            $table->index('floor');
            $table->index('type');
            $table->index('room_number');
            $table->index('room_number_in_floor');
            $table->index('count_room');
            $table->index('view');
            $table->index('area');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('apartments');
    }
}
