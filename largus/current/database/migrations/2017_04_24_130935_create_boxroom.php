<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBoxroom extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('boxroom', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('porch'); // подъезд
            $table->integer('unique_num'); // уникальный № кладовой
            $table->float('area');
            $table->integer('price');
            $table->integer('total_price');
            $table->integer('status');
            $table->integer('lead');
            $table->timestamps();

            $table->index('unique_num');
            $table->index('price');
            $table->index('total_price');
            $table->index('area');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('boxroom');
    }
}
