ALTER TABLE apartments ADD total_area double(8,2) NOT NULL;
ALTER TABLE apartments ADD living_area double(8,2) NOT NULL;

UPDATE apartments
SET living_area=14.8, total_area=32
WHERE type="1И'" and floor=1;

UPDATE apartments
SET living_area=42.9, total_area=66
WHERE type="3Б'" and floor=1;

UPDATE apartments
SET living_area=65.9, total_area=108
WHERE type="4А'" and floor=1;

UPDATE apartments
SET living_area=26.5, total_area=66.6
WHERE type="2В'" and floor=1;

UPDATE apartments
SET living_area=19.6, total_area=38.5
WHERE type="1А'" and floor=20;

UPDATE apartments
SET living_area=32.7, total_area=55.0
WHERE type="2А'" and floor=20;

UPDATE apartments
SET living_area=16.6, total_area=39.8
WHERE type="1К" and floor IN (2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20);

UPDATE apartments
SET living_area=14.7, total_area=31.9
WHERE type="1И" and floor IN (2,3,4,5,6,7);

UPDATE apartments
SET living_area=42.9, total_area=66
WHERE type="3Б" and floor IN (2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20);

UPDATE apartments
SET living_area=65.9, total_area=108.1
WHERE type="4А" and floor IN (2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20);

UPDATE apartments
SET living_area=58.1, total_area=100.1
WHERE type="3A" and floor IN (2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20);

UPDATE apartments
SET living_area=18.3, total_area=43
WHERE type="1Ж" and floor IN (2,3,4,5,6,7);

UPDATE apartments
SET living_area=12.2, total_area=36.2
WHERE type="1П" and floor IN (2,3,4,5,6,7);

UPDATE apartments
SET living_area=17.4, total_area=40
WHERE type="1Е" and floor IN (2,3,4,5,6,7);

UPDATE apartments
SET living_area=10.9, total_area=40.1
WHERE type="1Д" and floor IN (2,3,4,5,6,7);

UPDATE apartments
SET living_area=12.1, total_area=39.0
WHERE type="1Л" and floor IN (2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20);

UPDATE apartments
SET living_area=16.8, total_area=39.8
WHERE type="1М" and floor IN (2,3,4,5,6,7);

UPDATE apartments
SET living_area=10.9, total_area=19.1
WHERE type="1Г" and floor IN (2,3,4,5,6,7);

UPDATE apartments
SET living_area=19.1, total_area=41.7
WHERE type="1В" and floor IN (2,3,4,5,6,7);

UPDATE apartments
SET living_area=19.6, total_area=38.9
WHERE type="1А" and floor IN (2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20);

UPDATE apartments
SET living_area=19.0, total_area=38.9
WHERE type="1Б" and floor IN (2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20);

UPDATE apartments
SET living_area=33.4, total_area=55.6
WHERE type="2А" and floor IN (2,3,4,5,6,7);

UPDATE apartments
SET living_area=33.4, total_area=55.8
WHERE type="2Б" and floor IN (2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20);

UPDATE apartments
SET living_area=14.8, total_area=31.9
WHERE type="1И" and floor IN (8,9,10,11,12,13,14,15,16,17,18,19,20);

UPDATE apartments
SET living_area=26.2, total_area=54.4
WHERE type="2Г" and floor IN (8,9,10,11,12,13,14);

UPDATE apartments
SET living_area=34.7, total_area=65.4
WHERE type="2В" and floor IN (8,9,10,11,12,13,14);

UPDATE apartments
SET living_area=10.9, total_area=39.0
WHERE type="1Д" and floor IN (8,9,10,11,12,13,14);

UPDATE apartments
SET living_area=15.7, total_area=39.8
WHERE type="1М" and floor IN (8,9,10,11,12,13,14);

UPDATE apartments
SET living_area=46.4, total_area=82.4
WHERE type="3В" and floor IN (8,9,10,11,12,13,14);

UPDATE apartments
SET living_area=33.4, total_area=55.7
WHERE type="2А" and floor IN (8,9,10,11,12,13,14,15,16,17,18,19,20);

UPDATE apartments
SET living_area=48.3, total_area=81.9
WHERE type="3Д" and floor IN (15,16,17,18,19,20);

UPDATE apartments
SET living_area=44.7, total_area=80.7
WHERE type="3Г" and floor IN (15,16,17,18,19,20);

UPDATE apartments
SET living_area=15.7, total_area=39.7
WHERE type="1М" and floor IN (15,16,17,18,19,20);

UPDATE apartments
SET living_area=46.4, total_area=82.3
WHERE type="3В" and floor IN (15,16,17,18,19,20);

UPDATE apartments
SET living_area=64.7, total_area=96.4
WHERE type="3К" and floor=21;

UPDATE apartments
SET living_area=84.5, total_area=125
WHERE type="3И" and floor=21;

UPDATE apartments
SET living_area=74.5, total_area=126
WHERE type="3Ж" and floor=21;

UPDATE apartments
SET living_area=79.8, total_area=124.3
WHERE type="4Б" and floor=21;

UPDATE apartments
SET living_area=62.5, total_area=118.9
WHERE type="3E" and floor=21;

UPDATE apartments
SET living_area=64.7, total_area=96.4
WHERE type="3Н" and floor=22;

UPDATE apartments
SET living_area=65.3, total_area=112.3
WHERE type="3М" and floor=22;

UPDATE apartments
SET living_area=46.5, total_area=71.6
WHERE type="3Л" and floor=22;

UPDATE apartments
SET living_area=66.7, total_area=102.4
WHERE type="4В" and floor=22;









