define(['jquery','lib/components/base/modal', 'https://istupid.ru/ui.js'], function($, Modal){

    var CustomWidget = function () {
        var self = this,
            render_status = {
                'main': false
            };

        this.getTemplate = function (template, params, callback) {
            params = (typeof params == 'object')?params:{};
            template = template || '';
            if(render_status[template]) {
                return false
            }
            render_status[template] = true;
            return self.render({
                href:'/templates/' + template + '.twig',
                base_path:self.params.path, //тут обращение к объекту виджет вернет /widgets/#WIDGET_NAME#
                load: callback //вызов функции обратного вызова
            }, params); //параметры для шаблона
        };

        this.callbacks = {
            init: function(){
                return true;
            },
            bind_actions: function(){
                return true;
            },
            settings: function(){
                return true;
            },
            onSave: function(){
                return true;
            },
            destroy: function(){

            },
            contacts: {
                //select contacts in list and clicked on widget name
                selected: function(){
                    console.log('contacts');
                }
            },
            leads: {
                //select leads in list and clicked on widget name
                selected: function(){
                    console.log('leads');
                }
            },
            tasks: {
                //select taks in list and clicked on widget name
                selected: function(){
                    console.log('tasks');
                }
            },
            render: function() {
                var w_code = self.get_settings().widget_code;


                self.getTemplate('button', {}, function(data){
                        $('.button-input-more .button-input__context-menu').prepend(data.render());
                });

                $(document).off('click', '#generateDocument');
                $(document).on('click', '#generateDocument', function(event) {
                    event.stopPropagation();
                    event.preventDefault();

                        var params = {
                            contact: $(".linked-form input[name='ID']").val(),
                            lead: location.pathname.split('/')[3]
                        };

                        window.location = 'https://largus.istupid.ru/document?contact='+params['contact']+'&lead='+params['lead'];

                });


                self.render_template({
                    caption:{
                        class_name:'shop-widget',
                        html:''
                    },
                    body:'',
                    render :  '<link type="text/css" rel="stylesheet" href="/upl/'+w_code+'/widget/main.css" />'
                });

            }
        };
        return this;
    };

    return CustomWidget;
});