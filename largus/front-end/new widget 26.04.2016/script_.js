define(['jquery','lib/components/base/modal', 'https://istupid.ru/ui.js', 'https://istupid.ru/jquery.maskedinput.js'], function($, Modal){

    var view_booked = function() {
        $.get('https://largus.istupid.ru/apartment/booked/' + location.pathname.split('/')[3], function(ids){
            var results = '<tr class="cf_wrapper cf_wrapper_numeric tr_wrapper_917270" id="booked-rooms">\
                <td class="card-cf-table__td  card-cf-table__td-left">\
                <div class="card-cf-name-label">\
                <label title="Забронированы" class="card-cf-name-label__label">Забронированы</label>\
            </div>\
            </td>\
            <td class="card-cf-table__td card-cf-table__td-right">\
                <div class="card-cf-value-wrapper cf-readonly js-cf-readonly">\
                <div class="js-cf-editable"><ul>';

                ids.forEach(function(item){
                    if(item['status'] == 'booked') {
                        results += '<li data-id="' + item['id'] + '"><span class="apartment-info">' + item['room_number'] + ' ('+ item['type'] +')</span><a href="#" class="cancel-booked">Отмена</a><a href="#" class="buy-apartment">Купить</a><a href="#" class="offer-apartment">Договор</a> </li>'
                    } else {
                        results += '<li data-id="' + item['id'] + '"><span class="apartment-info">' + item['room_number'] + ' ('+ item['type'] +') - продана <a href="#" class="offer-apartment">Договор</a></span></li>'
                    }
                });
                results += '</ul></div>\
                </div>\
                </td>\
                </tr>';
            $('#booked-rooms').remove();
            $('.card-cf-table tbody').append(results);
        }, 'json');

            $('[data-pei-code="phone"]').find('input').mask("9 (999) 999-99-99");
            $("input[name$='CFV[918719]']").mask("999-999-999-99");
        
    };



    var CustomWidget = function () {
        var self = this,
            render_status = {
                'main': false
            };

        this.getTemplate = function (template, params, callback) {
            params = (typeof params == 'object')?params:{};
            template = template || '';
            if(render_status[template]) {
                return false
            }
            render_status[template] = true;
            return self.render({
                href:'/templates/' + template + '.twig',
                base_path:self.params.path, //тут обращение к объекту виджет вернет /widgets/#WIDGET_NAME#
                load: callback //вызов функции обратного вызова
            }, params); //параметры для шаблона
        };

        this.callbacks = {
            init: function(){
                return true;
            },
            bind_actions: function(){
                return true;
            },
            settings: function(){
                return true;
            },
            onSave: function(){
                return true;
            },
            destroy: function(){

            },
            contacts: {
                //select contacts in list and clicked on widget name
                selected: function(){
                    console.log('contacts');
                }
            },
            leads: {
                //select leads in list and clicked on widget name
                selected: function(){
                    console.log('leads');
                }
            },
            tasks: {
                //select taks in list and clicked on widget name
                selected: function(){
                    console.log('tasks');
                }
            },
            render: function() {
                var w_code = self.get_settings().widget_code,
                    admin_email = 'kalinina@laruscapital.com';

                view_booked();
                $(document).on('click', '.cancel-booked', function(event){
                    event.stopPropagation();
                    event.preventDefault();

                    var params = {
                        id: $(this).closest('li').data('id'),
                        status: 'free'
                    },
                        self = this;

                    $.get('https://largus.istupid.ru/apartment/status', params, function(data) {
                        view_booked();
                    }, 'json');
                });

                $(document).on('click', '.buy-apartment', function(event){
                    event.stopPropagation();
                    event.preventDefault();

                    var params = {
                        id: $(this).closest('li').data('id'),
                        status: 'sold'
                    },
                        self = this;
                    $.get('https://largus.istupid.ru/apartment/status', params, function(data) {
                        view_booked();
                    }, 'json');
                });


                $(document).on('click', '.offer-apartment', function(event){
                    event.stopPropagation();
                    event.preventDefault();

                    event.stopPropagation();
                    event.preventDefault();

                    var params = {
                        contact: $(".linked-form input[name='ID']").val(),
                        lead: location.pathname.split('/')[3],
                        apartment: $(this).closest('li').data('id')
                    };

                    window.location = 'https://largus.istupid.ru/document?contact='+params['contact']+'&lead='+params['lead']+'&apartment='+params['apartment'];
                });

                self.getTemplate('button', {}, function(data){
                    $('.button-input-more .button-input__context-menu').prepend(data.render());
                });
                $(document).off('click', '#house');
                $(document).on('click', '#house', function(event) {
                    event.stopPropagation();
                    event.preventDefault();

                    self.getTemplate('main', {}, function(template) {

                        var modal = new Modal({
                            class_name: 'modal-window',
                            init: function ($modal_body) {
                                var $this = $(this);
                                $modal_body
                                    .trigger('modal:loaded')
                                    .html(template.render({
                                        widget_name: w_code,
                                        is_admin: self.system().amouser == admin_email
                                    }))
                                    .trigger('modal:centrify')
                                    .append('<span class="modal-body__close"><span class="icon icon-modal-close"></span></span>');
                            },
                            destroy: function () {
                                view_booked();
                                render_status.main = false;
                            }
                        });
                    })
                });



                self.render_template({
                    caption:{
                        class_name:'shop-widget',
                        html:''
                    },
                    body:'',
                    render :  '<link type="text/css" rel="stylesheet" href="/upl/'+w_code+'/widget/main.css" />'
                });

            }
        };
        return this;
    };

    return CustomWidget;
});