<?php
namespace Deployer;
require 'recipe/laravel.php';

// Configuration

set('ssh_type', 'native');
set('ssh_multiplexing', true);
set('writable_mode', 'chmod');
set('writable_use_sudo', true);

set('repository', 'git@github.com:shaelf/s_aerealty.git');
//set('branch', 'develop');
set('branch', 'develop');
set('writable_chmod_mode', '777');
add('shared_files', []);
add('shared_dirs', ['public/documents/templates', 'public/documents/complete']);

set('writable_dirs', [
    'bootstrap/cache',
    'storage',
    'storage/app',
    'storage/framework',
    'storage/framework/cache',
    'storage/framework/sessions',
    'storage/framework/views',
    'storage/logs',
    'public/documents/templates',
    'public/documents/complete'
]);


// Servers

server('stage', 's.aerealty.ru')
    ->user('deploy')
    ->identityFile()
    ->set('deploy_path', '/home/deploy/www/s_realty');

server('production', 's.aerealty.ru')
    ->user('deploy')
    ->identityFile()
    ->set('deploy_path', '/home/deploy/www/s_realty');
// Tasks

desc('Restart PHP-FPM service');
task('php-fpm:restart', function () {
    // The user must have rights for restart service
    // /etc/sudoers: username ALL=NOPASSWD:/bin/systemctl restart php-fpm.service
    run('sudo /etc/init.d/php5-fpm restart');
});


after('deploy:symlink', 'php-fpm:restart');

// [Optional] if deploy fails automatically unlock.
after('deploy:failed', 'deploy:unlock');

// Migrate database before symlink new release.

before('deploy:symlink', 'artisan:migrate');