<table>
    <tr>
        <th>Корпус</th>
        <th>Подъезд</th>
        <th>Этаж</th>
        <th>Уникальный</th>
        <th>На этаже</th>
        <th>Кол-во комнат</th>
        <th>Общая площадь</th>
        <th>Цена за м2</th>
        <th>Стоимость</th>
        <th>Статус</th>
    </tr>
    @foreach($data as $id => $row)
    <tr>
        <td>{{$row['building_number']}}</td>
        <td>{{$row['section_number']}}</td>
        <td>{{$row['floor_number']}}</td>
        <td>{{$row['number']}}</td>
        <td>{{$row['number_on_floor']}}</td>
        <td>{{$row['room_count']}}</td>
        <td>{{$row['square']}}</td>
        <td>{{$row['cost_per_meter']}}</td>
        <td>{{$row['total_cost']}}</td>
        <td>{{$row['status']}}</td>
    </tr>
    @endforeach
</table>