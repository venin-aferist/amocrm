<div class="list__body-right__top">
    <div class="content__top__sidebar__toggler">
        <span id="sidebar_toggler" class="content__top__preset__filter_icon"><span class="icon icon-list filter-toggle-icon"></span></span>
    </div>
    <div class="content__top__preset">
        <h2 class="content__top__preset__caption" data-deleted="">
                    <span class="content__top__preset__caption__list__item content__top__preset__caption__list__item-active">
                        Роли
                    </span>
        </h2>
    </div>
</div>

<form id="settings_role_form" action="#" method="post" data-url="{{env('APP_URL')}}/roles">
    <table class="content__account__settings">
        <tbody>
        @foreach($users as $id => $data)
        <tr>
            <td class="content__account__settings__title">{{$data['name']}}</td>
            <td class="content__account__settings__field">
                <select name="roles[{{$id}}]">
                    @foreach(['admin' => 'Администратор', 'rop' => 'Руководитель', 'manager' => 'Менеджер'] as $tip => $title)
                    <option value="{{$tip}}" <?php if(isset($data['role']) && $tip == $data['role']) {echo 'selected';} ?>>{{$title}}</option>
                    @endforeach
                </select>
            </td>
        </tr>
        @endforeach
        </tbody></table>
    <div class="content__account__note__wrapper content__account__section_head bottom errors" style="display: none">
        <div class="content__account__note__wrapper content__account__section_head bottom errors" style="display: none">
            <div class="content__account__note content__account__section_head bottom errors" style="display: none">
                <p class="warning"></p>
            </div>
        </div>
    </div>
    <div>
        <button type="submit" class="button-input button-input_blue" tabindex="" id="save_role_button">
            <span class="button-input-inner "><span class="button-input-inner__text">Сохранить</span></span>
        </button>
        <button type="reset" class="button-input   button-cancel" tabindex="" id="" style="display: inline-block;">
            <span class="button-input-inner "><span class="button-input-inner__text">Отмена</span></span>
        </button>
    </div>
</form>

<div class="list__body-right__top">
    <div class="content__top__sidebar__toggler">
        <span id="sidebar_toggler" class="content__top__preset__filter_icon"><span class="icon icon-list filter-toggle-icon"></span></span>
    </div>
    <div class="content__top__preset">
        <h2 class="content__top__preset__caption" data-deleted="">
                    <span class="content__top__preset__caption__list__item content__top__preset__caption__list__item-active">
                        Документы
                    </span>
        </h2>
    </div>
</div>

<ul style="margin: 10px 20px 60px 20px;">
<?php
$templatePath = public_path() . '/documents/templates/';
foreach(glob("{$templatePath}*.docx") as $filename) {
    echo '<li>' . explode('.', basename($filename))[0] . '</li>';
}
?>
</ul>
<form action="https://s.aerealty.ru/templates" method="post" enctype="multipart/form-data">
    <table class="content__account__settings">
        <tbody>
            <tr>
                <td class="content__account__settings__title">Договор</td>
                <td class="content__account__settings__field">
                    <input type="file" name="template" />
                </td>
            </tr>
        </tbody></table>
    <div class="content__account__note__wrapper content__account__section_head bottom errors" style="display: none">
        <div class="content__account__note__wrapper content__account__section_head bottom errors" style="display: none">
            <div class="content__account__note content__account__section_head bottom errors" style="display: none">
                <p class="warning"></p>
            </div>
        </div>
    </div>
    <div>
        <button type="submit" class="button-input button-input_blue" tabindex="" id="save_document_button">
            <span class="button-input-inner "><span class="button-input-inner__text">Сохранить</span></span>
        </button>
        <button type="reset" class="button-input   button-cancel" tabindex="" id="" style="display: inline-block;">
            <span class="button-input-inner "><span class="button-input-inner__text">Отмена</span></span>
        </button>
    </div>
</form>

<script>
    $('#settings_role_form').on('submit', function(event) {
        event.preventDefault();
        event.stopPropagation();
        return false;
    });

    $('#save_role_button').on('click', function(event) {
        event.preventDefault();
        event.stopPropagation();
        var data = [],
            form = $(this).closest('form');

        form.find('select').each(function(item) {
            data[$(this).attr('name')] = $(this).val();
        });
        $.post($(form).data('url'), $( "#save_role_button" ).closest('form').serialize(), function(response) {
            console.log(response);
        }, 'json');
        return false;
    });
</script>