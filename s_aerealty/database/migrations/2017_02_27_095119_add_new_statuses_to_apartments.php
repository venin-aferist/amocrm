<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewStatusesToApartments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("ALTER TABLE apartments CHANGE COLUMN status status ENUM('free', 'booked', 'ibooked', 'sold', 'hold', 'register')");
//        Schema::table('apartments', function ($table) {
//            $table->enum('status', array())->default('free')->change();;
//        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
