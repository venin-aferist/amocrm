<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangePricesRowInApartmentsFromIntToFloat extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("ALTER TABLE apartments CHANGE COLUMN credit_sum credit_sum FLOAT");
        DB::statement("ALTER TABLE apartments CHANGE COLUMN cash_sum cash_sum FLOAT");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("ALTER TABLE apartments CHANGE COLUMN credit_sum credit_sum INT");
        DB::statement("ALTER TABLE apartments CHANGE COLUMN cash_sum cash_sum INT");
    }
}
