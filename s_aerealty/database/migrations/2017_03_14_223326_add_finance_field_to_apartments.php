<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFinanceFieldToApartments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('apartments', function($table){
            $table->integer('days');
            $table->integer('cash_sum');
            $table->integer('credit_sum');
            $table->date('date_agreement');
            $table->date('date_agreement_mortgage');
            $table->string('agreement_mortgage');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('apartments', function($table){
            $table->dropColumn('days');
            $table->dropColumn('cash_sum');
            $table->dropColumn('credit_sum');
            $table->dropColumn('date_agreement');
            $table->dropColumn('date_agreement_mortgage');
            $table->dropColumn('agreement_mortgage');
        });
    }
}