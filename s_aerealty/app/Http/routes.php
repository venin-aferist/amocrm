<?php

Route::get('/', function () {
//    return view('welcome');
    redirect('http://timpark.ru');
});
Route::get('/templates', 'DocumentController@index');

Route::get('/form', 'ApartmentsController@form');
Route::get('/roles', 'RoleController@index');
Route::get('/roles/role', 'RoleController@role');
Route::post('/roles', 'RoleController@update');
Route::get('/apartment/search', 'ApartmentsController@search');
Route::post('/apartment/import', 'ApartmentsController@import');
Route::post('/apartment/addpay', 'ApartmentsController@addpay');
//Route::get('/apartment/change', 'ApartmentsController@change');
Route::get('/apartment/status', 'ApartmentsController@status');
Route::get('/apartment/booked/{lead}', 'ApartmentsController@booked');
Route::get('/apartment/export{extension?}', 'ApartmentsController@exports');
Route::get('/order', 'OrderController@index');

Route::resource('apartment', 'ApartmentsController');


Route::post('/hook/status', 'HookController@status');

Route::get('document', 'DocumentController@generate');
Route::get('document/form', 'DocumentController@form');
Route::get('/templates', 'DocumentController@index');
Route::post('/templates', 'DocumentController@upload');

