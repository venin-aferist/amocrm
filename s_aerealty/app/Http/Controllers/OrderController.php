<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Api;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $amo = Api::connect();
        $data = $this->getOrders();
        $orderLastId = file_get_contents(base_path().'/public/orderLastId.txt');
        if(!empty($orderLastId)){
            $newLastId = end($data)['reqID'];
            reset($data);
            file_put_contents(base_path().'/public/orderLastId.txt', $newLastId);
        }else{
            file_put_contents(base_path().'/public/orderLastId.txt', 0);
            $oldLastId = 0;
        }
        $account = $amo->getAccountInfo();
        $contacts_cf = $account['response']['account']['custom_fields']['contacts'];
        $leadId = $account['response']['account']['leads_statuses'][0]['id'];
        // id для сделки со статусом ПЕРВИЧНЫЙ КОНТАКТ
        /**
         *
         */
        if(empty($data)) return;
        foreach ($data as $item) {
            if($item['reqID'] <= $oldLastId)continue;
            if(empty($item['reqContact'])) continue;
            $replaceList = ['(', ')', ' ', '-', '+'];
            $item['reqContact'] = str_replace($replaceList, '', $item['reqContact']);
            preg_match('/Имя:(.*)\s*Телефон/', $item['reqText'], $matches);
            $item['reqName'] = $matches[1];
            $contactIsset = $amo->getContactByQuery($item['reqContact']);
            if(!empty($contactIsset))continue;

            $newContactId = $this->addContact($amo, $item, $leadId, $contacts_cf);

            if(!isset($newContactId[0]['id']))continue;

            $newLeadId = $this->addLead($amo, $item, $leadId);
            if (isset($newContactId) && isset($newLeadId)) {
                $this->updateContact($amo, $newContactId, $newLeadId);
            }
            $amo->addComment($newLeadId, $item['reqText']);
        }

    }

    /**
     * @return mixed
     */
    private function getOrders(){
        $POST['password'] = env('art3d_PASS');
        $POST['login'] = env('art3d_LOGIN');
        $POST['action'] = 'getFeedbacksJSON';
        //$POST['clear'] = true; //Передать true если необходимо очистить базу от заявок
        $POST['clear'] = env('art3d_CLEAR');
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, 'http://legendarniy.ru/feedbacks/jsonpostserver');
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($POST));
        $out = curl_exec($curl);
        curl_close($curl);
        return json_decode($out, true);
    }

    /**
     * @param $amo
     * @param $item
     * @param $leadId
     * @param $contacts_cf
     * @return mixed
     */
    private function addContact($amo, $item, $leadId, $contacts_cf)
    {
        $newContactId = $amo->addContact([
            'name' => $item['reqName'],
            'status_id' => $leadId,
            'custom_fields' => [
                [
                    'id' => $contacts_cf[1]['id'], // телефон
                    'values' => [
                        ['value' => $item['reqContact'], 'enum' => 'MOB']
                    ]
                ],
            ],
            'last_modified' => $item['reqData']
        ]);
        return $newContactId;
    }

    /**
     * @param $amo
     * @param $item
     * @param $leadId
     * @return mixed
     */
    private function addLead($amo, $item, $leadId)
    {
        $newLeadId = $amo->addLead([
            'name' => 'Заявка с сайта ' . $item['reqContact'],
            'status_id' => $leadId,
            'last_modified' => time() + 10
        ]);
        return $newLeadId;
    }

    /**
     * @param $amo
     * @param $newContactId
     * @param $newLeadId
     */
    private function updateContact($amo, $newContactId, $newLeadId)
    {
        $amo->updateContact([
            'id' => $newContactId[0]['id'],
            'last_modified' => time() + 100,
            'linked_leads_id' => [$newLeadId[0]['id']]
        ]);
    }

}
