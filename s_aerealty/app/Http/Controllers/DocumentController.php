<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Apartment;
use App\Document;
use App\Api;
use Illuminate\Support\Facades\Redirect;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Log;
use App\Documents\Template;


class DocumentController extends Controller
{

    public function index(Request $request)
    {
        $templatePath = public_path() . '/documents/templates/';
        $templates = [];
        foreach(glob("{$templatePath}*.docx") as $filename) {
            $name = basename($filename);
            $templates[] = explode('.', $name)[0];
        }

        return \Response::json($templates);
    }

    public function upload(Request $request)
    {
        $request->file('template')
                ->move(public_path() . '/documents/templates/',
                       $request->file('template')->getClientOriginalName());
        return Redirect::back();
    }

    public function form(Request $request)
    {
        return view('document.form');
    }

    public function generate(Request $request)
    {
        $apartment = Apartment::find($request->apartment);
        $apartment = $this->updateApartment($apartment, $request);
        $amo = \App\Api::connect();
        $lead = $amo->getLead($request->leadId);
        $client = $amo->getContact($request->contactId);
        $templateProcessor = new Template(public_path() . '/documents/templates/' . $request->template . '.docx');


        $doc = new Document();
        $data = $this->buildData($templateProcessor, $apartment, $lead, $client);
        $data = $doc->applyHelpersFor($data);
        $filePath = public_path() . '/documents/complete/';
        $templateProcessor->apply($data);
        $fileName = $request->template . '-'.$request->leadId . '.docx';
        $templateProcessor->save($filePath . $fileName);

        $comment = 'Договор: ' . $request->template . "\n, ссылка для скачивания: " . url() . '/documents/complete/' . $fileName;
        $amo->addComment($request->leadId, $comment);

        return response()->download($filePath . $fileName);
    }

    private function buildData($templateProcessor, $apartament, $lead, $contact, $company = null)
    {
        $data = [];

        $data['leads::Название'] = $lead->name;
        $data['leads::Бюджет'] = $lead->price;
        $data['leads::id'] = $lead->id;
        foreach((array) $lead->custom_fields as $field) {
            $data['leads::' . $field->name] = $field->values[0]->value;
        }

        $data['contacts::Имя'] = $contact->name;
        foreach((array) $contact->custom_fields as $field) {
            $data['contacts::' . $field->name] = $field->values[0]->value;
        }
        $data['contacts::Инициалы'] = $this->getShortName($contact->name);

//        $data['companies::Название'] = $company->name;
//        foreach((array) $company->custom_fields as $field) {
//            $data['companies::' . $field->name] = $field->values[0]->value;
//        }

        $apartamentFields = ['Цена' => 'total_price',
                             'Цена за метр' => 'price',
                             'Этаж' => 'floor',
                             'Кол-во комнат' => 'count_room',
                             'Площадь' => 'area',
                             'Номер' => 'room_number',
                             'Номер секции' => 'porch',
                             'Номер на этаже' => 'room_number_in_floor',
                             'Срок' => 'days',
                             'Дата договора' => 'date_agreement',
                             'Корпус' => 'housing',
                             'Сумма наличными' => 'cash_sum',
                             'Сумма кредита' => 'credit_sum',
                             'Дата ипотечного договора' => 'date_agreement_mortgage',
                             'Номер ипотечного договора' => 'agreement_mortgage',
                             'Номер кредитного счета' => 'credit_bank_account_number',
                             'Скидка' => 'discount'
        ];

        foreach($apartamentFields as $key => $value) {
            $data['apartment::' . $key] = $apartament->{$value};
        }

        $agreementNumber = 'Не определен';
        if($apartament->date_agreement && strtotime($apartament->date_agreement)) {
            $agreementNumber = "БА/К" . $apartament->housing . "/с" . $apartament->porch . "/";
            $agreementNumber .= $apartament->room_number . "/" . date('dmy', strtotime($apartament->date_agreement));
        }
        $data['apartment::Номер договора'] = $agreementNumber;
        $data['apartment::Стоимость брони'] = $apartament->reservePrice();
        $data['apartment::Цена'] = $apartament->finalPrice();
        $data['apartment::Цена за метр'] = $apartament->finalMeterPrice();
        $this->applyFloorLayout($templateProcessor, $apartament, ['width' => '200', 'height' => '100']);

        return $data;
    }

    private function updateApartment($apartment, Request $request)
    {
        $available = ['days', 'credit_sum', 'date_agreement',
                        'date_agreement_mortgage', 'agreement_mortgage',
                        'credit_bank_account_number', 'discount'];


        foreach($available as $field){
            if(isset($request->{$field}) && !empty($request->{$field})) {
                $apartment->{$field} = $request->{$field};
            }
        }

        if(isset($apartment->credit_sum) && $apartment->credit_sum > 0) {
            $apartment->cash_sum = $apartment->finalPrice() - $apartment->credit_sum;
        } else {
            $apartment->cash_sum = $apartment->finalPrice();
        }

        $apartment->save();

        return $apartment;
    }

    private function applyFloorLayout($template, $apartment, $params)
    {
        $imageLink = base_path().'/public/static/layouts/' . $apartment->housing . '-' . $apartment->floor . '.png';
        if(file_exists($imageLink))
            $template->replaceStrToImg('apartment::План этажа', $imageLink, $params);
    }

    /**
     * Get short name
     *
     * @param $name
     * @return string
     */
    private function getShortName($name)
    {
        $parts = explode(' ', $name);
        $name = $parts[0];
        if(isset($parts[1])) {
            $name .= " ". mb_substr($parts[1], 0, 1) .".";
        }

        if(isset($parts[2])) {
            $name .= " ". mb_substr($parts[2], 0, 1) .".";
        }

        return $name;
    }
}