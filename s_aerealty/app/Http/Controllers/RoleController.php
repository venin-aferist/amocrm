<?php
/**
 * Created by PhpStorm.
 * User: shaelf
 * Date: 19/02/2017
 * Time: 11:23
 */

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Symfony\Component\HttpFoundation\Response;
use App\Api;

class RoleController extends Controller
{
    public function update(Request $request)
    {
        foreach($request->roles as $id => $role) {
            $r = Role::where('user_id', '=', $id)->first();
            if(!isset($r->id)) {
                $r = new Role();
                $r->user_id = $id;
            }
            $r->role = $role;
            $r->save();
        }
        return \Response::json(['status' => 'ok']);
    }

    public function index(Request $request)
    {
        $api = new Api();
        $users = [];
        $roles = Role::all();
        foreach($api->info()->response->account->users as $user) {
            $users[$user->id] = ['name' => $user->name];
        }

        foreach($roles as $role) {
            if(isset($users[$role->user_id])) {
                $users[$role->user_id]['role'] = $role->role;
            }
        }
        return view('role.index', ['users' => $users]);
    }

    public function role(Request $request)
    {
        $api = new Api();
        $user_role = 'manager';
        foreach($api->info()->response->account->users as $user) {
            if($request->email == $user->login) {
                $role = Role::where('user_id', '=', $user->id)->get()->first();
                if($role) {
                    $user_role = $role->role;
                }
            }
        }

        return \Response::json(['role' => $user_role]);
    }
}