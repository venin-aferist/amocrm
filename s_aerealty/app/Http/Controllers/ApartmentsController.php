<?php

namespace App\Http\Controllers;

use App\Parsers\Dinvest;
use App\Parsers\Dinvest\Data;
use App\Parsers\Timpark;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Apartment;
use App\Api;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\View;
use Symfony\Component\HttpFoundation\Response;

class ApartmentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $result = [];
        $apartments = Apartment::orderBy('floor', 'DESC')->get();
        foreach($apartments as $item) {
            if($item->booked_for) {
                $item->booked_for = date('d.m.Y', strtotime($item->booked_for));
            }
            $result[] = $item;
        }
        return \Response::json($result);
    }

    public function search(Request $request)
    {
        $result = [];
        if($request->get('result_count')) {
            $result = Apartment::findBy($request->all())->orderBy('floor', 'DESC')->count();
        } else {
            $orderBy = isset($request->sort_by) ? $request->sort_by : 'floor';
            $order = isset($request->order) ? strtoupper($request->order) : 'DESC';
            $data = Apartment::findBy($request->except(['order_by', 'order']))->orderBy($orderBy, $order)->get();
            foreach($data as $item) {
                if($item->booked_for) {
                    $item->booked_for = date('d.m.Y', strtotime($item->booked_for));
                }
                $result[] = $item;
            }
        }

        return \Response::json($result);
    }

    public function booked(Request $request, $lead)
    {
        return \Response::json(Apartment::getBookedByLead($lead));
    }

    public function import(Request $request, Response $response)
    {
        $data = Dinvest::parse($request->file('import'));
        foreach($data as $element) {
            if(!$element->hasValid()) {
                continue;
            }
            $apartment = Apartment::where('housing', '=', $element->buildingNumber)
                                    ->where('room_number', '=', $element->room_number)->get();
            if($apartment->first()) {
                //update
                $apartment = $apartment->first();
                if($apartment->status == Apartment::STATUS_FREE || $apartment->status == Apartment::STATUS_HOLD) {
                    $apartment->price = Data::formatPrice($apartment->price_for_meter);
                    $apartment->total_price = Data::formatPrice($apartment->price);
                }
                $apartment->new_total_price = Data::formatPrice($apartment->price);
                $apartment->new_price = Data::formatPrice($apartment->price_for_meter);
            } else {
                //add new
                $apartment = new Apartment();
                $apartment->housing = $element->buildingNumber;
                $apartment->status = $element->status;
                $apartment->porch = $element->section;
                $apartment->floor = $element->floor_number;
                $apartment->room_number = $element->room_number;
                $apartment->room_number_in_floor = $element->section_number;
                $apartment->count_room = $element->count_room;
                $apartment->area = $element->shared_square;
                $apartment->total_price = Data::formatPrice($element->price);
                $apartment->price = Data::formatPrice($element->price_for_meter);
                $apartment->new_total_price = Data::formatPrice($element->price);
                $apartment->new_price = Data::formatPrice($element->price_for_meter);
                $apartment->booked_for = $element->reserve_data;
            }

            $apartment->save();
        }

    }

    public function exports(Request $request)
    {
        $data = ($request->ids) ? Apartment::whereIn('id', explode(',', $request->ids))->get() : Apartment::all();
        $result = array();
        foreach($data as $item) {
            $result[] = array(
                'building_number'  => $item->housing,
                'section_number'   => $item->porch,
                'floor_number'     => $item->floor,
                'number'           => $item->room_number,
                'number_on_floor'  => $item->room_number_in_floor,
                'room_count'       => $item->count_room,
                'square'           => $item->area,
                'status'           => $this->statusAsNumber($item->status),
                'total_cost'       => $item->total_price,
                'cost_per_meter'   => $item->price,
                'type'             => $item->type,
                'level_count'      => $item->level_count,
                'note'             => ''
            );
        }

        $forSaleExport = [];
        foreach($data as $item) {
            $forSaleExport[] = [
                'Корпус' => $item->housing,
                'Подъезд' => $item->porch,
                'Этаж' => $item->floor,
                'Уникальный' => $item->room_number,
                'На этаже' => $item->room_number_in_floor,
                'Кол-во комнат' => $item->count_room,
                'Общая площадь' => $item->area,
                'Цена за м2' => $item->price,
                'Стоимость' => $item->total_price,
                'Статус' => $this->readableStatus($item->status)
            ];
        }

        switch($request->extension) {
            case '.xslx':
                \Excel::create('result', function($excel) use($forSaleExport) {
                    $excel->sheet('Результат', function($sheet) use($forSaleExport){
                       $sheet->fromArray($forSaleExport);
                    });
                })->download('xlsx');
                break;
            case '.pdf':
                $mpdf = new \mPDF('utf-8', 'A4', '10', 'Arial', 0, 0, 5, 5, 5, 5);
                $mpdf->charset_in = 'utf-8';

                $stylesheet = 'table {
                    text-align: center;
                    width: 320px;
                    color: black;
                    margin: 0;
                    float: left;
                    border-collapse: collapse;
               }
               td {
                    width: 80px;
               }
               th {border-bottom: 1px solid black}';

                $mpdf->WriteHTML($stylesheet, 1);
                $mpdf->list_indent_first_level = 0;
                $mpdf->WriteHTML((string) View::make('apartments.pdf', ['data' => $result])->render(), 2);
                $mpdf->Output('apartments.pdf', 'D');
                break;
            default:
                return \Response::json($result);
                break;
        }
    }

    public function form(Request $request)
    {
        return view('apartments.form');
    }

    public function status(Request $request)
    {
        $apartment = Apartment::find($request->get('id'));

        $leadId = ($apartment->lead > 0) ? $apartment->lead : $request->get('lead');

        if($request->get('status') == Apartment::STATUS_BOOKED) {
            if (Apartment::where('lead', $request->get('lead'))->count() >= 3) {
                return \Response::json(array('error' => 'Забронированно максимальное количество'));
            }

            $maxDays = 60 * 60 * 24 * 3;
            $timestamp = strtotime($request->get('date'));

            if($timestamp > time() + $maxDays) {
                return \Response::json(array('error' => 'Максимальный срок брони 3 дня'));
            } elseif((time() - 60 * 60 * 24) > $timestamp) {
                return \Response::json(array('error' => 'Нельзя выбрать ранюю дату'));
            }

            $date = date('Y-m-d', $timestamp);
            $apartment->booked_for = $date;
            $apartment->lead = $request->get('lead');
            $apartment->status = $request->get('status');
            $apartment->deal_name = $request->get('deal_name');
            $apartment->save();
        } elseif($request->get('status') == Apartment::STATUS_IBOOKED) {
            if (Apartment::where('lead', $request->get('lead'))->count() >= 3) {
                return \Response::json(array('error' => 'Забронированно максимальное количество'));
            }
            $maxDays = 60 * 60 * 24 * 15;
            $timestamp = strtotime($request->get('date'));

            if($timestamp > time() + $maxDays) {
                return \Response::json(array('error' => 'Максимальный срок брони 15 дней'));
            } elseif((time() - 60 * 60 * 24) > $timestamp) {
                return \Response::json(array('error' => 'Нельзя выбрать ранюю дату'));
            }

            $date = date('Y-m-d', $timestamp);
            $apartment->booked_for = $date;
            $apartment->lead = $request->get('lead');
            $apartment->status = $request->get('status');
            $apartment->deal_name = $request->get('deal_name');
            $apartment->save();
        } elseif($request->get('status') == Apartment::STATUS_SOLD) {
            $apartment->deal_name = '';
            $apartment->status = Apartment::STATUS_SOLD;
            $apartment->save();
        } elseif($request->get('status') == Apartment::STATUS_REGISTER) {
            if (Apartment::where('lead', $request->get('lead'))->count() >= 3) {
                return \Response::json(array('error' => 'Забронированно максимальное количество'));
            }

            $apartment->lead = $request->get('lead');
            $apartment->status = $request->get('status');
            $apartment->deal_name = $request->get('deal_name');
            $apartment->save();
        } else {
            $apartment->status = $request->get('status');
            $apartment->price = $apartment->new_price;
            $apartment->total_price = $apartment->new_total_price;
            $apartment->lead = '';
            $apartment->deal_name = '';
            $apartment->save();
        }

        $price = 0;
        foreach(Apartment::getBookedByLead($leadId) as $a) {
            $price += $a->total_price;
        }


//
//        $this->getAmo()->setLeads($updatePrice);
        $amo = \App\Api::connect();
        Log::debug($amo->updateLead([
            'price' => $price,
            'id'    => $leadId,
            'last_modified' => time() + 10
        ]));

        return \Response::json($apartment);
    }

    private function statusAsNumber($status)
    {
        switch($status) {
            case Apartment::STATUS_BOOKED:
                return 2;
                break;
            case Apartment::STATUS_IBOOKED;
                return 2;
                break;
            case Apartment::STATUS_REGISTER;
                return 2;
                break;
            case Apartment::STATUS_FREE:
                return 1;
                break;
            case Apartment::STATUS_SOLD:
                return 0;
                break;
            case Apartment::STATUS_HOLD:
                return 0;
                break;
        }
    }

    private function readableStatus($status)
    {
        switch($status) {
            case Apartment::STATUS_BOOKED:
                return 'Бронь';
                break;
            case Apartment::STATUS_IBOOKED;
                return 'Ипот. бронь';
                break;
            case Apartment::STATUS_REGISTER;
                return 'Зарегистрирован';
                break;
            case Apartment::STATUS_FREE:
                return 'Свободная';
                break;
            case Apartment::STATUS_SOLD:
                return 'Продана';
                break;
            case Apartment::STATUS_HOLD:
                return 'Снята с продажи';
                break;
        }
    }

    public function change(Request $request)
    {
        foreach(Apartment::all() as $apartment) {
            if($apartment->room_number > 80) {
                $apartment->room_number = $apartment->room_number -2;
                $apartment->save();
            }
        }

    }
    

    
    

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


}
