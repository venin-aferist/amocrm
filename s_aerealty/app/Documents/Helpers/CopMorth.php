<?php

/**
 * Created by PhpStorm.
 * User: shaelf
 * Date: 28/02/2017
 * Time: 10:18
 */

namespace App\Documents\Helpers;

use App\Documents\IHelper;

class CopMorth implements IHelper
{
    public static function execute($value)
    {
        $value = explode('.', $value);
        $value = (isset($value[1])) ? $value[1] : 0;
        return NumberToString::execute($value)['sum'];
    }
}