<?php
/**
 * Created by PhpStorm.
 * User: shaelf
 * Date: 06/03/2017
 * Time: 11:02
 */

namespace App\Documents\Helpers;


use App\Documents\IHelper;

class NormalizeDate implements IHelper
{
    public static function execute($value)
    {
        if(!is_int($value)) {
            $value = strtotime($value);
        }
        return strftime('%d.%m.%Y', $value);
    }
}

