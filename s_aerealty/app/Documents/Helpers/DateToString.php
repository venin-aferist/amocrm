<?php
/**
 * Created by PhpStorm.
 * User: shaelf
 * Date: 28/02/2017
 * Time: 10:29
 */

namespace App\Documents\Helpers;

use App\Documents\IHelper;

class DateToString implements IHelper
{
    public static function execute($value)
    {
        if(!is_int($value)) {
            $value = strtotime($value);
        }
        $month = ['Января', 'Февраля', 'Марта', 'Апреля', 'Мая', 'Июня', 'Июля', 'Августа', 'Сентября', 'Октября', 'Ноября', 'Декабря'];
        $month = $month[date("m", $value)-1];
        return strftime('«%e» ', $value) . $month . strftime(' %Y года', $value);
    }
}