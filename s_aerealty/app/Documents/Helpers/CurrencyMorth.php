<?php

/**
 * Created by PhpStorm.
 * User: shaelf
 * Date: 28/02/2017
 * Time: 10:18
 */

namespace App\Documents\Helpers;

use App\Documents\IHelper;

class CurrencyMorth implements IHelper
{
    public static function execute($value)
    {
        return NumberToString::execute($value)['sum'];
    }
}