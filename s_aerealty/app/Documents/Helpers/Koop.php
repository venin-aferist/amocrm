<?php
/**
 * Created by PhpStorm.
 * User: shaelf
 * Date: 19/03/2017
 * Time: 14:21
 */

namespace App\Documents\Helpers;

use App\Documents\IHelper;

class Koop implements IHelper
{
    public static function execute($value)
    {
        $value = str_replace(',', '.', strval($value));
        $value = explode('.',$value);

        return (!isset($value[1])) ? '00' : $value[1];
    }
}