<?php

/**
 * Created by PhpStorm.
 * User: shaelf
 * Date: 28/02/2017
 * Time: 10:18
 */

namespace App\Documents\Helpers;

use App\Documents\IHelper;

class SquareToString implements IHelper
{
    public static function execute($value)
    {
        $value = explode('.', $value);
        $result = NumberToString::execute($value[0])['sum'];
        if(isset($value[1])) {
            $result .= ' целых ' . NumberToString::execute($value[1])['sum'] . ' сотых';
        }

        return $result;
    }

}