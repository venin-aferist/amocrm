<?php

/**
 * Created by PhpStorm.
 * User: shaelf
 * Date: 28/02/2017
 * Time: 10:22
 */

namespace App\Documents;

interface IHelper
{
    public static function execute($value);
}