<?php
/**
 * Created by PhpStorm.
 * User: shaelf
 * Date: 09.03.16
 * Time: 13:04
 */

namespace App;

class Api
{

    public static function connect()
    {
        return new Api();
    }

    public function info()
    {
        return json_decode($this->response('accounts/current', null, 'GET'));
    }

    public function addComment($leadId, $comment) {
        $request = [
            'request' => [
                'notes' => [
                    'add' => [[
                        'element_type' => 2,
                        'element_id' => (int)$leadId,
                        "note_type" =>  4,
                        'text' => $comment
                    ]]
                ]
            ]
        ];
        $this->response('notes/set', $request, 'POST');
    }

    public function getLead($leadId)
    {
        return json_decode($this->response('leads/list?id[]=' . $leadId, [], 'GET'))->response->leads[0];
    }

    public function getLeads()
    {
        return json_decode($this->response('leads/list', [], 'GET'), true);
    }

    public function getContact($contactId)
    {
        return json_decode($this->response('contacts/list?id[]=' . $contactId, [], 'GET'))->response->contacts[0];
    }

    public function getContactByQuery($query)
    {
        return json_decode($this->response('contacts/list?query=' . $query, [], 'GET'), true)['response']['contacts'][0];
    }

    public function getContacts()
    {
        return json_decode($this->response('contacts/list', [], 'GET'), true);
    }

    public function getCompany($companyId)
    {
        return json_decode($this->response('company/list?id[]=' . $companyId, [], 'GET'))->response->contacts[0];
    }

    public function getAccountInfo()
    {
        return json_decode($this->response('accounts/current', [], 'GET'), true);
    }

    public function updateLead($params)
    {
        $updatePrice['request']['leads']['update'] = [$params];
        return $this->response('leads/set', $updatePrice);
    }

    public function addLead($params)
    {
        $addPrice['request']['leads']['add'] = [$params];
        return json_decode($this->response('leads/set', $addPrice), true)['response']['leads']['add'];
    }

    public function addContact($params){
        $addContact['request']['contacts']['add'] = [$params];
        return json_decode($this->response('contacts/set', $addContact), true)['response']['contacts']['add'];
    }

    public function updateContact($params){
        $updateContact['request']['contacts']['update'] = [$params];
        return $this->response('contacts/set', $updateContact);
    }

    private function response($link, $data, $method = 'POST') {
        sleep(1);
        $link = 'https://' . env('AMO_DOMAIN') . '.amocrm.ru/private/api/v2/json/'. $link;
        if(strpos($link, '?') > 0) {
            $link .= '&USER_LOGIN=' . env('AMO_LOGIN') . '&USER_HASH=' . env('AMO_API');
        } else {
            $link .= '?USER_LOGIN=' . env('AMO_LOGIN') . '&USER_HASH=' . env('AMO_API');
        }
            $curl=curl_init();
        curl_setopt($curl,CURLOPT_RETURNTRANSFER,true);
        curl_setopt($curl,CURLOPT_USERAGENT,'amoCRM-API-client/1.0');
        curl_setopt($curl,CURLOPT_URL, $link);

        if (!empty($method))
        {
            curl_setopt($curl,CURLOPT_CUSTOMREQUEST,$method);
        }
        if (!empty($data))
        {
            curl_setopt($curl,CURLOPT_POSTFIELDS,json_encode($data));
        }
        curl_setopt($curl,CURLOPT_HTTPHEADER,array('Content-Type: application/json'));
        curl_setopt($curl,CURLOPT_HEADER,false);
        curl_setopt($curl,CURLOPT_COOKIEFILE,dirname(__FILE__).'/cookie.txt'); #PHP>5.3.6 dirname(__FILE__) -> __DIR__
        curl_setopt($curl,CURLOPT_COOKIEJAR,dirname(__FILE__).'/cookie.txt'); #PHP>5.3.6 dirname(__FILE__) -> __DIR__
        curl_setopt($curl,CURLOPT_SSL_VERIFYPEER,0);
        curl_setopt($curl,CURLOPT_SSL_VERIFYHOST,0);

        $out=curl_exec($curl); #Инициируем запрос к API и сохраняем ответ в переменную
        $code=curl_getinfo($curl,CURLINFO_HTTP_CODE); #Получим HTTP-код ответа сервера
        curl_close($curl); #Завершаем сеанс cURL
        return $out;
    }


}