<?php
/**
 * Created by PhpStorm.
 * User: shaelf
 * Date: 25/03/2017
 * Time: 11:37
 */

namespace App\Parsers;


class Dinvest
{
    static public function parse($file)
    {
        $dinvest = new Dinvest($file);
        return $dinvest->init();
    }

    public function __construct($file)
    {
        $this->file = $file;
    }


    protected function init()
    {
        $rv = [];
        if (($handle = fopen($this->file->getPath() . '/' . $this->file->getFilename(), "r")) !== FALSE) {
            $excel = \PHPExcel_IOFactory::load($this->file);
            $worksheet = $excel->getSheet(0)->toArray(null,true,true,true);
            foreach($worksheet as $rowNumber => $row) {
                $data = new \App\Parsers\Dinvest\Data();
                foreach($row as $collNumber => $col) {
                    $data->add($col);
                }
                $rv[] = $data;
            }
        }

        return $rv;
    }

    private function getStatus($id)
    {
        return ($id == '1') ? Apartment::STATUS_FREE : Apartment::STATUS_BOOKED;
    }
}