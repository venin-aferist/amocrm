<?php
/**
 * Created by PhpStorm.
 * User: shaelf
 * Date: 19/02/2017
 * Time: 17:03
 */
namespace App\Parsers;

use App\Apartment;

class Timpark
{
    static public function parse($file)
    {
        $timpark = new Timpark($file);
        $timpark->init();
    }

    public function __construct($file)
    {
        $this->file = $file;
    }


    protected function init()
    {
        if (($handle = fopen($this->file->getPath() . '/' . $this->file->getFilename(), "r")) !== FALSE) {
            $flatsArray = explode("\n", file_get_contents($this->file));
            foreach ($flatsArray as $flat) {
                $fields = explode(";", $flat);
                if (count($fields) > 4) {
                    if(((int) $fields[3]) > 0) {
                        $exist = Apartment::byRoomNumber($fields[3]);
                        $price      = (int) str_replace(['₽', "В ", ' ', " "], '', $fields[9]);
                        $totalPrice = (int) str_replace(['₽', "В ", " ", ' '], '', $fields[8]);

                        if($exist) {
                            if($exist->status == Apartment::STATUS_FREE || $exist->status == Apartment::STATUS_HOLD) {
                                $exist->price = $price;
                                $exist->total_price = $totalPrice;
                            }
                            $exist->new_total_price = $totalPrice;
                            $exist->new_price = $price;


                            $exist->housing = $fields[0];
                            $exist->porch = $fields[1];
                            $exist->floor = $fields[2];
                            $exist->room_number_in_floor = $fields[4];
                            $exist->count_room = $fields[5];
                            $exist->area = str_replace(',', '.', $fields[6]);
                            $exist->view = $fields[11];
                            $exist->level_count = $fields[12];
                            $exist->type = iconv('MacCyrillic', 'UTF-8', $fields[10]);
                            $exist->save();
                        } else {
                            //вид последний
                            $apartment = new Apartment();
                            $apartment->housing = $fields[0];
                            $apartment->porch = $fields[1];
                            $apartment->floor = $fields[2];
                            $apartment->room_number = $fields[3];
                            $apartment->room_number_in_floor = $fields[4];
                            $apartment->count_room = $fields[5];
                            $apartment->view = $fields[11];
                            $apartment->level_count = $fields[12];
                            $apartment->area = $fields[6];
                            $apartment->status = self::getStatus($fields[7]);
                            $apartment->total_price = $totalPrice;
                            $apartment->price = $price;
                            $apartment->type = iconv('MacCyrillic', 'UTF-8', $fields[10]);
                            $apartment->new_total_price = $totalPrice;
                            $apartment->new_price = $price;
                            $apartment->save();
                        }

                    }
                }
            }
            fclose($handle);
        }
    }

    private function getStatus($id)
    {
        return ($id == '1') ? Apartment::STATUS_FREE : Apartment::STATUS_BOOKED;
    }

}
