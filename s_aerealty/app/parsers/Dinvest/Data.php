<?php

/**
 * Created by PhpStorm.
 * User: shaelf
 * Date: 19/02/2017
 * Time: 21:38
 */

namespace App\Parsers\Dinvest;

class Data
{
    public $floor_number = 0;

    public $section = 0;

    public $buildingNumber = 0;

    public $section_number = 0;

    public $room_number = 0;

    public $count_room = 0;


    public $shared_square = 0;

    public $price_for_meter = 0;

    public $price = 0;

    public $status = '';

    public $reserve_data = '';

    public $agent = '';

    public $view = '';

    public $type = '';


    private $counter = 0;

    /**
     * Add value to variable by queue
     *
     * @param $value
     */
    public function add($value)
    {
        $key = $this->next();
        if ($key) {
            if ($key == 'count_room' || $key == 'buildingNumber') {
                $value = (int)$value;
            }

            if($key == 'status') {
                $this->setStatus($value);
            } else {
                $this->{$key} = $value;
            }
        }
        $this->counter++;
    }

    public function hasValid()
    {
        return (int) $this->price > 0 && (isset($this->room_number));
    }

    public static function formatPrice($value)
    {
        return round(str_replace([',', ' '], '', $value));
    }

    private function next() {
        $list = $this->getList();
        return (isset($list[$this->counter])) ? $list[$this->counter] : null;
    }

    private function getList()
    {
        return ['', '', 'buildingNumber', '', 'section', 'floor_number', 'section_number', 'count_room', 'type', 'room_number', 'shared_square', 'price_for_meter',
            'price', 'status', 'view'];
    }

    private function setStatus($value)
    {
        $parts = explode(' ', $value);
        $this->status = $this->getStatusByLabel($parts[0]);
        var_export($this->status);
        var_export($value);
    }

    /**
     * Get status by label from import file
     *
     * @param $label
     * @return string
     */
    private function getStatusByLabel($label)
    {
        switch(mb_strtolower($label)) {
            case 'фиксация':
                return 'sold';
            case 'контракт':
                return 'sold';
            case 'свободно':
                return 'free';
            default:
                return 'free';
        }
    }

}