<?php
/**
 * Created by PhpStorm.
 * User: shaelf
 * Date: 19/02/2017
 * Time: 17:15
 */

namespace App\Parsers;

require __DIR__ . '/../../vendor/phpoffice/phpexcel/Classes/PHPExcel/IOFactory.php';

class Checkboard
{
    static public function parse($file)
    {
        $timpark = new Checkboard($file);
        return $timpark->init();
    }

    public function __construct($file)
    {
        $this->file = $file;
    }


    protected function init()
    {
        $rv = [];
        if (($handle = fopen($this->file->getPath() . '/' . $this->file->getFilename(), "r")) !== FALSE) {
            $excel = \PHPExcel_IOFactory::load($this->file);
            $worksheet = $excel->getSheet(2)->toArray(null,true,true,true);
            $startParse = false;
            $data = [];
            $floorNumber = 0;
            $section = [];
            $sectionNumber = 0;
            $oldSectionNumber = 0;
            $buildingNumber = 0;
            foreach($worksheet as $rowNumber => $row) {
                foreach($row as $collNumber => $col) {
                    //Get building number
                    if($rowNumber == 2 && !empty($col)) {
                        $buildingNumber = (int) $col;
                    }
                    //Calculate section number
                    if($rowNumber == '6') {
                        $tmp = (int) str_replace('Секция', '', $col);
                        if($tmp != 0) {
                            $oldSectionNumber = $tmp;
                        }
                        $section[$collNumber] = $oldSectionNumber;
                    }
                    //Check start document parsing
                    if(!$startParse) {
                        if ($col == 'Этаж') {
                            $startParse = true;
                        }
                        continue;
                    }

                    if(!$startParse) {
                        continue;
                    }


                    if($collNumber == 'B' && $col != null && ((int) $col > 0)) {
                        $floorNumber = (int) $col;
                    }

                    if(!isset($data[$section[$collNumber]][$floorNumber])) {
                        $data[$section[$collNumber]][$floorNumber] = [];
                    }

                    if(is_numeric($worksheet[7][$collNumber])) {
                        $sectionNumber = (int) $worksheet[7][$collNumber];
                    }

                    if (!isset($data[$section[$collNumber]][$floorNumber][$sectionNumber])) {
                        $apartamentData = new \App\Parsers\Checkboards\Data();
                        $apartamentData->floor_number = $floorNumber;
                        $apartamentData->section_number = $sectionNumber;
                        $apartamentData->buildingNumber = $buildingNumber;
                        $apartamentData->section = $section[$collNumber];
                        $data[$section[$collNumber]][$floorNumber][$sectionNumber] = $apartamentData;
                    }

                    $data[$section[$collNumber]][$floorNumber][$sectionNumber]->add($col);
                }
            }

            foreach($data as $elements) {
                foreach($elements as $element) {
                    foreach($element as $item) {
                        $rv[] = $item;
                    }
                }
            }
        }


        return $rv;
    }

    private function getStatus($id)
    {
        return ($id == '1') ? Apartment::STATUS_FREE : Apartment::STATUS_BOOKED;
    }
}