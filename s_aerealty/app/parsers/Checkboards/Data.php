<?php

/**
 * Created by PhpStorm.
 * User: shaelf
 * Date: 19/02/2017
 * Time: 21:38
 */

namespace App\Parsers\Checkboards;

class Data
{
    public $floor_number = 0;

    public $section = 0;

    public $buildingNumber = 0;

    public $section_number = 0;

    public $room_number = 0;

    public $count_room = 0;

    public $client = '';

    public $shared_square = 0;

    public $price_for_meter = 0;

    public $price = 0;

    public $status = '';

    public $reserve_data = '';

    public $agent = '';

    private $counter = 0;

    /**
     * Add value to variable by queue
     *
     * @param $value
     */
    public function add($value)
    {
        $key = $this->next();
        if($key == 'client' && is_numeric(str_replace(',', '', $value))) {
            $this->counter++;
            $key = $this->next();
        }
        if ($key) {
            if ($key == 'count_room') {
                $value = (int)$value;
            }

            if($key == 'status') {
                $this->setStatus($value);
            } else {
                $this->{$key} = $value;
            }
        }
        $this->counter++;
    }

    public function hasValid()
    {
        return (int) $this->price > 0;
    }

    public static function formatPrice($value)
    {
        return round(str_replace([',', ' '], '', $value));
    }

    private function next() {
        $list = $this->getList();
//        var_export($list[$this->counter]);
        return isset($list[$this->counter]) ? $list[$this->counter] : 'agent';
    }

    private function validateType($value)
    {
        $types = ['string', 'int', 'int', 'miss', 'string', 'int', 'int', 'string', 'string'];
        $rv = false;
        switch($types[$this->counter]) {
            case 'string':
                $rv = is_string($value);
                break;
            case 'int':
                $rv = is_numeric($value);
                break;
            case 'miss':
                $rv = true;
                break;
        }

        return $rv;
    }

    private function getList()
    {
        return ['room_number', 'count_room', 'shared_square', '', 'client', '', '', '', 'price_for_meter', '',
            'price', '',  'status', '', 'agent', ''];
    }

    private function setStatus($value)
    {
        $parts = explode(' ', $value);
        $this->status = $this->getStatusByLabel($parts[0]);
//        if(isset($parts[2])) {
//            $this->reserve_data = date('Y-m-d',strtotime($parts[2]));
//        }
    }

    /**
     * Get status by label from import file
     *
     * @param $label
     * @return string
     */
    private function getStatusByLabel($label)
    {
        switch($label) {
            case 'Фиксация':
                return 'sold';
            case 'Контракт':
                return 'sold';
            case 'Свободно':
                return 'free';
            default:
                return 'free';
        }
    }

}