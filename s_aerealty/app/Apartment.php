<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Apartment extends Model
{
    const STATUS_FREE   = 'free';
    const STATUS_BOOKED = 'booked';
    const STATUS_IBOOKED = 'ibooked';
    const STATUS_SOLD   = 'sold';
    const STATUS_HOLD   = 'hold';
    const STATUS_REGISTER = 'register';

    public static function findBy($params) {
        $range_available = ['price', 'total_price', 'area', 'floor',];
        $equal_available = ['type', 'housing', 'porch', 'count_room', 'room_number', 'view', 'status', 'room_number_in_floor'];

        $rv = self::where([]);
        if(isset($params['room_number'])) {
            return $rv->where('room_number', $params['room_number']);
        }

        foreach($range_available as $field) {
            if(isset($params[$field . '_from']) && !isset($params[$field . '_to'])) {
                $rv = $rv->where($field, '>=', $params[$field . '_from']);
            }

            if(!isset($params[$field . '_from']) && isset($params[$field . '_to'])) {
                $rv = $rv->where($field, '<=', $params[$field . '_to']);
            }

            if(isset($params[$field . '_from']) && isset($params[$field . '_to'])) {
                $rv = $rv->where($field, '>=', $params[$field . '_from'])->where($field, '<=', $params[$field . '_to']);
            }
        }

        foreach($equal_available as $field) {
            if(isset($params[$field])) {
                if($field == 'view') {
                    $params[$field]--;
                }
                if($field == 'count_room' && $params[$field] == 'Студия') {
                    $params[$field] = 0;
                }
                $rv = $rv->where($field, $params[$field]);
            }
        }

        return $rv;
    }

    public static function byRoomNumber($roomNumber)
    {
        return self::where('room_number', $roomNumber)->first();
    }

    public static function fromExcel($fields)
    {

    }

    public static function getBookedByLead($lead)
    {
        $result = [];
        $statuses = [Apartment::STATUS_BOOKED, Apartment::STATUS_SOLD,
                     Apartment::STATUS_IBOOKED, Apartment::STATUS_REGISTER];
        $data = Apartment::whereIn('status', $statuses)->where('lead', $lead)->get();
        foreach($data as $item) {
            if($item->booked_for) {
                $item->booked_for = date('d.m.Y', strtotime($item->booked_for));
            }
            $result[] = $item;
        }

        return $result;
    }

    /**
     * Get final price after calculate discount
     *
     * @return int
     */
    public function finalPrice()
    {
        if($this->attributes['discount'] > 0) {
            return $this->attributes['total_price'] - ($this->attributes['total_price'] / 100 * $this->attributes['discount']);
        }

        return $this->attributes['total_price'];
    }

    /**
     * Get final price after calculate discount
     *
     * @return int
     */
    public function finalMeterPrice()
    {
        if($this->attributes['discount'] > 0) {
            return $this->attributes['price'] - ($this->attributes['price'] / 100 * $this->attributes['discount']);
        }

        return $this->attributes['price'];
    }

    /**
     * Get price for reserve
     *
     * @return float
     */
    public function reservePrice()
    {
        return $this->finalPrice() / 100 * 3.5;
    }


}
