<?php

namespace App;

class Document
{

    public static function applyHelpersFor(array $data)
    {
        $enableHelpers = ['CurrencyMorth', 'DateToString', 'NormalizeDate',
                          'CopMorth', 'SquareToString', 'Rubble', 'Koop'];
        $rv = [];
        foreach($data as $key => $value) {
            $rv[$key] = $value;
            foreach($enableHelpers as $helper) {
                $helperClass = '\App\Documents\Helpers\\' . $helper;
                $rv[$key . '::' . $helper] = $helperClass::execute($value);
            }
        }

        return $rv;
    }

    /**
     * Get russian mouth name by mouth number
     *
     * @param Int $num
     * @return String
     */
	public function num2month($num) {
		if ($num > 12) {
			$num = $num - 12;
		}
        return ['января', 'февраля', 'марта', 'апреля', 'мая', 'июня',
                'июля', 'августа', 'сентября', 'октября', 'ноября', 'декабря'][$num - 1];
	}

    public function num2str($num) {
        $nul='ноль';
        $ten=array(
            array('','один','два','три','четыре','пять','шесть','семь', 'восемь','девять'),
            array('','одна','две','три','четыре','пять','шесть','семь', 'восемь','девять'),
        );
        $a20=array('десять','одиннадцать','двенадцать','тринадцать','четырнадцать' ,'пятнадцать','шестнадцать','семнадцать','восемнадцать','девятнадцать');
        $tens=array(2=>'двадцать','тридцать','сорок','пятьдесят','шестьдесят','семьдесят' ,'восемьдесят','девяносто');
        $hundred=array('','сто','двести','триста','четыреста','пятьсот','шестьсот', 'семьсот','восемьсот','девятьсот');
        $unit=array( // Units
            array('копейка' ,'копейки' ,'копеек',	 1),
            array('рубль'   ,'рубля'   ,'рублей'    ,0),
            array('тысяча'  ,'тысячи'  ,'тысяч'     ,1),
            array('миллион' ,'миллиона','миллионов' ,0),
            array('миллиард','милиарда','миллиардов',0),
        );
        //
        list($rub,$kop) = explode('.',sprintf("%015.2f", floatval($num)));

        $out = array();
        if (intval($rub)>0) {
            foreach(str_split($rub,3) as $uk=>$v) { // by 3 symbols
                if (!intval($v)) continue;
                $uk = sizeof($unit)-$uk-1; // unit key
                $gender = $unit[$uk][3];

                list($i1,$i2,$i3) = array_map('intval',str_split($v,1));
                // mega-logic
                $out[] = $hundred[$i1]; # 1xx-9xx
                if ($i2>1) $out[]= $tens[$i2].' '.$ten[$gender][$i3]; # 20-99
                else $out[]= $i2>0 ? $a20[$i3] : $ten[$gender][$i3]; # 10-19 | 1-9
                // units without rub & kop
                if ($uk>1) $out[]= $this->morph($v,$unit[$uk][0],$unit[$uk][1],$unit[$uk][2]);
            } //foreach

        }
        else $out[] = $nul;
        $out[] =  // rub
            // $out[] = $kop.' '.$this->morph($kop,$unit[0][0],$unit[0][1],$unit[0][2]); // kop
        $res['sum'] = $this->myucfirst(trim(preg_replace('/ {2,}/', ' ', join(' ',$out))));
        $res['currency'] = $this->morph(intval($rub), $unit[1][0],$unit[1][1],$unit[1][2]);
        //$res['sum'][0] = mb_strtoupper($res['sum'][0]);
        return $res;
    }

    /**
     * Склоняем словоформу
     * @ author runcore
     */
    private function morph($n, $f1, $f2, $f5) {
        $n = abs(intval($n)) % 100;
        if ($n>10 && $n<20) return $f5;
        $n = $n % 10;
        if ($n>1 && $n<5) return $f2;
        if ($n==1) return $f1;
        return $f5;
    }
    

    private	function myucfirst($str, $enc = 'utf-8') {
    	return mb_strtoupper(mb_substr($str, 0, 1, $enc), $enc).mb_substr($str, 1, mb_strlen($str, $enc), $enc);
    }
    
}
